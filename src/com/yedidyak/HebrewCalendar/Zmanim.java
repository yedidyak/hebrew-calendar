package com.yedidyak.HebrewCalendar;

import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Zmanim {

    public class Zman{
        public String colorKey;
        public String showKey;
        public boolean defaultShow;
        public int defaultColor;
        public Date time;

        private Zman(String showKey, String colorKey, boolean defaultShow, int defaultColor, Date time){
            this.colorKey = colorKey;
            this.showKey = showKey;
            this.defaultColor = defaultColor;
            this.defaultShow = defaultShow;
            this.time = time;
        }
    }

    public List<Zman> getZmanim(ZmanimCalendar day){
        List<Zman> zmanim = new ArrayList<Zman>();

        JewishCalendar jcal = new JewishCalendar(day.getCalendar());

        if(jcal.isErevYomTov() || jcal.getDayOfWeek()==Calendar.FRIDAY){
            zmanim.add(new Zman("show_candle_lighting", "candle_lighting_color", false, 0xff4c4c4c , day.getCandleLighting()));
        }

        zmanim.add(new Zman("show_dawn", "dawn_color", false, 0xff308014 , day.getAlosHashachar()));
        zmanim.add(new Zman("show_sunrise", "sunrise_color", false, 0xff00ff00 , day.getSunrise()));
        zmanim.add(new Zman("show_sof_shma_mga", "sof_shma_mga_color", false, 0xff00ff00 , day.getSofZmanShmaMGA()));
        zmanim.add(new Zman("show_sof_shma_gra", "sof_shma_gra_color", false, 0xff00ff00 , day.getSofZmanShmaGRA()));
        zmanim.add(new Zman("show_sof_tfila_mga", "sof_tfila_mga_color", false, 0xff308014 , day.getSofZmanTfilaMGA()));
        zmanim.add(new Zman("show_sof_tfila_gra", "sof_tfila_gra_color", false, 0xff308014 , day.getSofZmanTfilaGRA()));
        zmanim.add(new Zman("show_chatzot", "chatzot_color", false, 0xFF808080 , day.getChatzos()));
        zmanim.add(new Zman("show_mincha_gedola", "mincha_gedola_color", false, 0xFF808080 , day.getMinchaGedola()));
        zmanim.add(new Zman("show_mincha_ketana", "mincha_ketana_color", false, 0xFF808080 , day.getMinchaKetana()));
        zmanim.add(new Zman("show_plag_mincha", "plag_mincha_color", false, 0xFF808080 , day.getPlagHamincha()));
        zmanim.add(new Zman("show_sunset", "sunset_color", false, 0xFFFF0000 , day.getSunset()));
        zmanim.add(new Zman("show_tzais", "tzais_color", false, 0xFF800000 , day.getTzais()));
        zmanim.add(new Zman("show_tzais_72", "tzais_72_color", false, 0xFF000000 , day.getTzais72()));

        return zmanim;
    }
}
