package com.yedidyak.HebrewCalendar.Month;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.yedidyak.HebrewCalendar.Day.SingleDayView;
import com.yedidyak.HebrewCalendar.Model.DayModel;

public class DayCell extends View implements SingleDayView {

    private int width;
    private int height;
    private Paint textPaint;
    private DayModel day = null;
    private Context context;
    private MonthAdapter adapter;

    public DayCell(Context context, MonthAdapter adapter, int width, int height) {
        super(context);
        this.width = width;
        this.height = height;
        init(context, adapter);
    }


    private void init(Context context, MonthAdapter adapter){

        this.context = context;
        this.adapter = adapter;
        textPaint = new Paint();
        textPaint.setTextSize(25);

        //this.setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();

        int startTextX = (int)(width*0.66);
        int startTextY = (int)(height*0.2);

        if (day == null){return;}

        String text = day.hebDateBefore;

        canvas.drawText(text, startTextX, startTextY, textPaint);
    }

    @Override
    public void setDay(DayModel day) {
        this.day = day;
        this.day.addView(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        setMeasuredDimension(width,height);
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        this.adapter.listView.invalidate();
        this.adapter.notifyDataSetChanged();
    }
}

