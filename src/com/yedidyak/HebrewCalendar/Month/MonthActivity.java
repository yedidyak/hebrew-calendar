package com.yedidyak.HebrewCalendar.Month;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.HebrewCalendar.AboutActivity;
import com.yedidyak.HebrewCalendar.ActionBarAdapter;
import com.yedidyak.HebrewCalendar.Day.DayActivity;
import com.yedidyak.HebrewCalendar.HebrewCalendar;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.R;
import com.yedidyak.HebrewCalendar.Week.WeekActivity;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;

import java.util.Calendar;
import java.util.Locale;

public class MonthActivity extends ListActivity implements ActionBar.OnNavigationListener, AbsListView.OnScrollListener{

    public final int DAY = 0;
    public final int WEEK = 1;
    public final int MONTH = 2;

    private int startOffset;

    private Calendar middleCal = Calendar.getInstance();

    private MonthAdapter adapter;
    //private ListView listView;
    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent i;
        switch (item.getItemId()) {
            case R.id.about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                return  true;
            default:
                return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.month_list_activity);
        //listView = (ListView)findViewById(android.R.id.list);

        startOffset = ((HebrewCalendar)getApplication()).currentMonth;

        ActionBarAdapter actionBarAdapter =  ActionBarAdapter.getInstance(this,
                android.R.layout.simple_dropdown_item_1line,
                getResources().getStringArray(R.array.action_items));

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setListNavigationCallbacks(actionBarAdapter, this);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setSelectedNavigationItem(MONTH);
        actionBar.show();

        adapter = new MonthAdapter(this, getLayoutInflater(), getListView(), startOffset);

        setListAdapter(adapter);
        getListView().setSelection((Integer.MAX_VALUE / 2));
        getListView().setOnScrollListener(this);
        getListView().setItemsCanFocus(false);

    }


    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        Intent i;
        switch (itemPosition){
            case DAY:
                i = new Intent(this, DayActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            case WEEK:
                i = new Intent(this, WeekActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            case MONTH:
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view,
                         int firstVisible, int visibleCount, int totalCount) {

        ((HebrewCalendar)getApplication()).currentMonth = (firstVisible+ visibleCount)-10000 + startOffset;

        boolean loadMore = /* maybe add a padding */
                firstVisible + visibleCount >= totalCount;

        if(loadMore) {
            adapter.setCount(adapter.getCount() + visibleCount);
            adapter.notifyDataSetChanged();
        }

        int middle = firstVisible + visibleCount / 2;
        WeekModel middleWeek = adapter.getItem(middle);

        if (middleWeek != null && middleWeek.getNumOfDays()>4){
            middleCal.setTime(middleWeek.getDay(4).zmanimCalendar.getSunset());
            JewishCalendar jcal = new JewishCalendar(middleCal);

            HebrewDateFormatter hdf = new HebrewDateFormatter();

            String number = "";

            if (Locale.getDefault().getLanguage().equals("iw")){
                hdf.setHebrewFormat(true);
                number = hdf.formatHebrewNumber(jcal.getJewishYear());
            }
            else{
                number = "" + jcal.getJewishYear();
            }

            String date = hdf.formatMonth(jcal) + " " + number;
            ActionBarAdapter.getCurrentInstance().setMainText(date);
            adapter.focusJewishMonth = jcal.getJewishMonth();
            adapter.notifyDataSetChanged();


        }
    }


}