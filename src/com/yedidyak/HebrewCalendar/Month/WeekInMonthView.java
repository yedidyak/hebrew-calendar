package com.yedidyak.HebrewCalendar.Month;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import com.yedidyak.HebrewCalendar.Model.Event;
import com.yedidyak.HebrewCalendar.Model.MonthWeekModel;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.Week.SingleWeekView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WeekInMonthView extends View implements SingleWeekView{

    private int height;
    private int width;
    public int dayWidth;
    public MonthWeekModel week = null;
    private Context context;
    private MonthAdapter adapter;
    private Paint dividerPaint = new Paint();
    private Paint dayNumPaint = new Paint();
    private Paint allDayPaint = new Paint();
    private Paint eventPaint = new Paint();
    private Paint backgroundPaint = new Paint();
    ///
    private Paint gregNumPaint = new Paint();
    private DateFormat gdf = new SimpleDateFormat("d/M");
    ///
    private RectF r = new RectF();
    private boolean selectedMonth = false;
    public int selectedDay = -1;

    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    Calendar now = Calendar.getInstance();

    private final int PAST_COLOR_EVEN = 0xFFE6E6E6;
    private final int PAST_COLOR_ODD = 0xFFEEEEEE;
    float hr;
    float min;

    public int pos;

    public WeekInMonthView(Context context, MonthAdapter adapter) {
        super(context);
        init(context, adapter);
    }


    private void init(Context context, MonthAdapter adapter){

        this.context = context;
        this.adapter = adapter;

        Resources r = getResources();
        height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, r.getDisplayMetrics());

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        dayWidth = width/7;

        hr = (float)(height*0.76)/24;
        min = hr/60;

        dividerPaint.setColor(Color.LTGRAY);
        dividerPaint.setStrokeWidth(1);

        gregNumPaint.setColor(Color.LTGRAY);
        gregNumPaint.setStrokeWidth(1);


        //dayNumPaint.setColor(Color.BLACK);
        dayNumPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, r.getDisplayMetrics()));
        gregNumPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, r.getDisplayMetrics()));

    }

    @Override
    public void setWeek(WeekModel week) {
        this.week = (MonthWeekModel)week;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(width, height);
    }

    @Override
    public void onDraw(Canvas canvas){
        drawBackground(canvas);
        drawDayDividers(canvas);
        drawDayNumbers(canvas);
        drawEvents(canvas);
    }

    private void drawBackground(Canvas canvas){

        if (this.week!=null && this.week.getNumOfDays() > 0)  {
            for (int i=0; i<7; i++){
                r.left  = i*dayWidth;
                r.right = r.left + dayWidth;
                r.top = 0;
                r.bottom = height;

                if (this.week!=null && this.week.getNumOfDays() > i){
                    if (selectedDay == i){backgroundPaint.setColor(Color.BLUE);}
                    else if (week.isToday[i]){backgroundPaint.setColor(Color.WHITE);}
                    else if (week.evenMonth[i]){backgroundPaint.setColor(PAST_COLOR_EVEN);}
                    else {backgroundPaint.setColor(PAST_COLOR_ODD);}
                    canvas.drawRect(r, backgroundPaint);
                }
            }
        }
    }

    private void drawDayDividers(Canvas canvas){
        for (int i=0; i<7; i++){
            float x = (i+1)*dayWidth;
            canvas.drawLine(x,0,x,height,dividerPaint);
        }
    }

    private void drawDayNumbers(Canvas canvas){

        if (week.JmonthOfFirstDay == adapter.focusJewishMonth){
            dayNumPaint.setFakeBoldText(true);
            selectedMonth = true;
        }
        else{
            dayNumPaint.setFakeBoldText(false);
            dayNumPaint.setColor(Color.DKGRAY);
            selectedMonth = false;
        }

        for (int i=0; i<7; i++){
            float x = (i*dayWidth) + (float)(0.6*dayWidth);
            float y = (float)0.3*height;

            float gy = (float)0.7*height;
            float gx = (float)(i*dayWidth) + (float)(0.5*dayWidth);

            if (this.week!=null && this.week.getNumOfDays() > i){
                if (i>0 && this.week.getDay(i).hebDateBefore.equals("1")){
                    dayNumPaint.setFakeBoldText(!selectedMonth);
                    selectedMonth = false;
                    if(selectedMonth) {
                        dayNumPaint.setColor(Color.BLACK);
                    } else {
                        dayNumPaint.setColor(Color.DKGRAY);
                    }
                }
                canvas.drawText(this.week.getDay(i).hebDateBefore,x,y,dayNumPaint);
                canvas.drawText(gdf.format(this.week.getDay(i).zmanimCalendar.getSunset().getTime()),gx,gy,gregNumPaint);
                //canvas.drawText(this.week.getDay(i).sunset.get(Calendar.DAY_OF_MONTH)+"",x,y2,dayNumPaint);
            }
        }
    }

    private void drawEvents(Canvas canvas){
        for (int i=0; i<7; i++){
            if (this.week!=null && this.week.getNumOfDays() > i){

                if(this.week.getDay(i).hasAllDayEvent()) {

                    float left = (i*dayWidth) + (float) 0.1*dayWidth;
                    float right = (i*dayWidth) + (float) 0.9*dayWidth;
                    float top = (float)0.05*height;
                    float bottom = (float)0.1*height;

                    if (this.week.getDay(i).getAllDayEvents().size() > 1){
                        allDayPaint.setColor(Color.BLACK);
                    }
                    else if (this.week.getDay(i).getAllDayEvents().size() > 0){
                        allDayPaint.setColor(this.week.getDay(i).getAllDayEvents().get(0).getColor());
                    }

                    canvas.drawRect(left, top, right, bottom, allDayPaint);
                }

                float left = (i*dayWidth)+(float)0.1*dayWidth;
                float right = (i*dayWidth)+(float)0.23*dayWidth;

                for (Event event : this.week.getDay(i).getEvents()){
                    eventPaint.setColor(event.getColor());
                    canvas.drawRect(left, calendarToY(event.getStart()), right, calendarToY(event.getEnd()), eventPaint);
                }


            }
        }
    }

    float calendarToY(Calendar cal){
        return (float)(height*0.12) + cal.get(Calendar.HOUR_OF_DAY) * hr + cal.get(Calendar.MINUTE) * min;
    }

}

