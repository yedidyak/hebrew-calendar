package com.yedidyak.HebrewCalendar.Month;

import android.content.Context;
import android.util.Pair;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.Week.WeekBar;
import com.yedidyak.HebrewCalendar.Week.WeekEventGetter;

public class MonthWeekGetter extends WeekEventGetter {

    private WeekInMonthView view;

    public MonthWeekGetter(Context mContext, WeekModel weekModel, WeekInMonthView viewWeek, WeekBar weekBar) {
        super(mContext, weekModel, viewWeek, weekBar);
        view = viewWeek;
    }

    @Override
    protected void onProgressUpdate(Pair<Integer, DayModel>... values) {
        super.onProgressUpdate(values);
        view.requestLayout();
        values[0].second.addView(view);
    }

    @Override
    protected void onPostExecute(Void v) {
    }
}

