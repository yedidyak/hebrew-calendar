package com.yedidyak.HebrewCalendar.Month;

import android.content.Context;
import android.content.Intent;
import android.view.*;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.yedidyak.HebrewCalendar.Day.DayActivity;
import com.yedidyak.HebrewCalendar.Model.MonthWeekModel;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.View.TapGestureDetector;

import java.util.Calendar;
import java.util.HashMap;

public class MonthAdapter extends BaseAdapter implements View.OnTouchListener{

    private int count;
    private Context context;
    private LayoutInflater inflater;
    public ListView listView;
    public int focusJewishMonth = 0;
    private int startOffset;
    private GestureDetector mGestureDetector;

    private Intent i = null;

    private HashMap<Integer, MonthWeekModel> weeks = new HashMap<Integer, MonthWeekModel>(100);

    public MonthAdapter(Context context, LayoutInflater inflater, ListView listView, int startOffset){
        this.context = context;
        this.count = 10001;
        this.inflater = inflater;
        this.listView = listView;
        this.startOffset = startOffset;
        mGestureDetector = new GestureDetector(context, new TapGestureDetector());
    }

    public void setCount(int count){
        this.count = count;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public WeekModel getItem(int position) {
        if (weeks.containsKey(position)){return weeks.get(position);}
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup p) {

        WeekInMonthView view = (WeekInMonthView)convertView;
        if(view == null){
            view = new WeekInMonthView(context, this);
        }

        MonthWeekModel weekModel;
        if (weeks.containsKey(pos)){
            weekModel = weeks.get(pos);
            view.setWeek(weekModel);
        }
        else{
            weekModel = new MonthWeekModel();
            Calendar calendar = Calendar.getInstance();
            int offset = pos-10000 + startOffset;
            calendar.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
            calendar.add(Calendar.WEEK_OF_YEAR, offset);
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND, 0);
            view.setWeek(weekModel);
            new MonthWeekGetter(context, weekModel, view, null).execute(calendar);
            weeks.put(pos, weekModel);
        }

        view.setOnTouchListener(this);
        //p.requestDisallowInterceptTouchEvent(true);

        return view;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        WeekInMonthView view = (WeekInMonthView)v;

        int dayTouched = (int)Math.floor((event.getX() / view.dayWidth));

        if (dayTouched == 7){dayTouched = 6;}

        if(mGestureDetector.onTouchEvent(event)){
            long time = view.week.getDay(dayTouched).zmanimCalendar.getSunset().getTime();
            Intent i = new Intent(context, DayActivity.class);
            i.putExtra("selected", time);
            context.startActivity(i);
        }

        return true;
    }
}

