package com.yedidyak.HebrewCalendar.Week;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.R;

import java.util.Calendar;

/**
 * Fragment which shows a week
 */
public class WeekFragment extends Fragment {

    private int weekOffset;
    private Context mContext;
    private View rootView;

    /**
     * Creates a weekView with the right week
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState!=null){
            this.weekOffset = savedInstanceState.getInt("offset");
        }
        else{
            weekOffset = (Integer)getArguments().get("offset");
        }

        //inflate the fragment layout (compound of LL and Scroll))
        rootView = inflater.inflate(R.layout.fragment, null);

        ScrollView scrl = (ScrollView) rootView.findViewById(R.id.scroll_view_day);
        LinearLayout llweekBar = (LinearLayout)rootView.findViewById(R.id.ll_week_bar);

        //Create a new WeekView and WeekBar
        ViewWeek viewWeek = new ViewWeek(mContext);
        WeekBar weekBar = new WeekBar(mContext);
        llweekBar.addView(weekBar);

        //make a calendar set to the start of the week to be shown
        Calendar c_start = Calendar.getInstance();
        //workaround... needed for some reason
        c_start.set(c_start.get(Calendar.YEAR), c_start.get(Calendar.MONTH), c_start.get(Calendar.DAY_OF_MONTH));
        ////////////////
        c_start.add(Calendar.WEEK_OF_YEAR, weekOffset);
        c_start.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        c_start.set(Calendar.HOUR_OF_DAY,0);
        c_start.set(Calendar.MINUTE,0);
        c_start.set(Calendar.SECOND,0);
        c_start.set(Calendar.MILLISECOND, 0);

        //make a new weekmodel
        WeekModel weekModel = new WeekModel();
        //asynchronously load the week data
        new WeekEventGetter(mContext, weekModel, viewWeek, weekBar).execute(c_start);
        //add the WeekView to the scroll
        scrl.addView(viewWeek);

        return rootView;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("offset", weekOffset);
        super.onSaveInstanceState(outState);
    }
}

