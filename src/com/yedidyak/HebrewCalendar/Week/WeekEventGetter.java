package com.yedidyak.HebrewCalendar.Week;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.util.Pair;

import com.yedidyak.HebrewCalendar.LocationGetter;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.Model.Event;
import com.yedidyak.HebrewCalendar.Model.WeekModel;

import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.text.DateFormat;
import java.util.*;

import static com.yedidyak.HebrewCalendar.Util.getTimeInUTC;

public class WeekEventGetter extends AsyncTask<Calendar, Pair<Integer, DayModel>, Void> {

    protected Context mContext;
    protected WeekModel model;
    protected SingleWeekView view;
    private WeekBar weekBar;

    public WeekEventGetter(Context mContext, WeekModel weekModel, SingleWeekView viewWeek, WeekBar weekBar) {
        this.mContext = mContext;
        this.model = weekModel;
        this.view = viewWeek;
        this.weekBar = weekBar;
    }

    @Override
    protected Void doInBackground(Calendar... calendars) {
        Calendar s_cal = calendars[0];

        //int current_dw = s_cal.get(Calendar.DAY_OF_WEEK);
        //int offset = current_dw-1;
        s_cal.set(s_cal.get(Calendar.YEAR), s_cal.get(Calendar.MONTH), s_cal.get(Calendar.DAY_OF_MONTH));
        s_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        //s_cal.add(Calendar.DAY_OF_WEEK, -1*offset);

        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(s_cal.getTime());
        e_cal.set(Calendar.HOUR_OF_DAY,23);
        e_cal.set(Calendar.MINUTE,59);
        e_cal.set(Calendar.SECOND,59);
        e_cal.set(Calendar.MILLISECOND,999);
        e_cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

        for (int day=0; day<7; day++){

            DateFormat df = android.text.format.DateFormat.getDateFormat(mContext);
            HebrewDateFormatter jdf = new HebrewDateFormatter();

            if (Locale.getDefault().getLanguage().equals("iw")){
                jdf.setHebrewFormat(true);
            }

            String hebBefore = "" + new JewishDate(s_cal).getJewishDayOfMonth();

            Calendar tmorrow = Calendar.getInstance();
            tmorrow.setTime(s_cal.getTime());
            tmorrow.add(Calendar.DAY_OF_YEAR, 1);
            String hebAfter = "" + new JewishDate(tmorrow).getJewishDayOfMonth();

            //Calendar sscal = LocationGetter.getInstance(mContext).getSunSet(e_cal);
            ZmanimCalendar zcal = new ZmanimCalendar(LocationGetter.getInstance(mContext).getGeoLocation());
            zcal.setCalendar(e_cal);
            DayModel dayModel = new DayModel(zcal, hebBefore, hebAfter);

            //new DayEventGetter(mContext, dayModel).execute((Calendar) s_cal.clone(), (Calendar) e_cal.clone());

            List<Event> events = new ArrayList<Event>();

            String selection = "((dtstart >= "+(s_cal.getTimeInMillis()-2)+") AND (dtend <= "+e_cal.getTimeInMillis()+"))";

            Cursor cursor = mContext.getContentResolver().query(
                    CalendarContract.Events.CONTENT_URI, Event.projection, selection, null, null
            );


            while (cursor.moveToNext()){
                dayModel.addEvent(new Event(cursor));
            }

            cursor.close();

            Calendar midnightUTC = Calendar.getInstance();
            midnightUTC.set(Calendar.DAY_OF_YEAR, s_cal.get(Calendar.DAY_OF_YEAR));
            midnightUTC.set(Calendar.YEAR, s_cal.get(Calendar.YEAR));
            midnightUTC.set(Calendar.HOUR, 0);
            midnightUTC.set(Calendar.MINUTE,0);
            midnightUTC.set(Calendar.MILLISECOND,0);

            cursor = fetchAllDayCursor(s_cal);

            while (cursor.moveToNext()){
                if(cursor.getString(Event.ind_ALL_DAY).equals("1")){
                    dayModel.addEvent(new Event(cursor));
                }
            }

            cursor.close();

            publishProgress(new Pair<Integer, DayModel>(day, dayModel));

            s_cal.add(Calendar.DAY_OF_MONTH, 1);
            e_cal.add(Calendar.DAY_OF_MONTH, 1);
        }

         return null;
    }

    @Override
    protected void onProgressUpdate(Pair<Integer, DayModel>... values) {
        DayModel dayModel = values[0].second;
        int day = values[0].first;
        model.setDayModel(dayModel, day);
    }

    @Override
    protected void onPostExecute(Void v) {
        this.view.setWeek(this.model);
        if(this.weekBar != null) {this.weekBar.setModel(this.model);}
    }

    public Cursor fetchAllDayCursor(Calendar start) {
        Calendar end = (Calendar) start.clone();
        end.add(Calendar.DATE, 1);

        long startTime = getTimeInUTC(start) + 1;
        long endTime = getTimeInUTC(end) - 1;

        ContentResolver resolver = mContext.getContentResolver();
        return CalendarContract.Instances.query(resolver, Event.instances_projection, startTime, endTime);
    }

}

