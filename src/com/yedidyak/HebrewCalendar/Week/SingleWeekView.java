package com.yedidyak.HebrewCalendar.Week;

import com.yedidyak.HebrewCalendar.Model.WeekModel;

public interface SingleWeekView{

    public void setWeek(WeekModel week);
}

