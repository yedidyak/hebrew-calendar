package com.yedidyak.HebrewCalendar.Week;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import com.yedidyak.HebrewCalendar.ActionBarAdapter;
import com.yedidyak.HebrewCalendar.HebrewCalendar;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.util.Calendar;
import java.util.Locale;

/**
 * Adapter for WeekFragments
 */
public class WeekPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

    //offset of the current week
    public static final int OFFSET = 5001;
    private Context context;
    //enum Days {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};

    /**
     * Constructor
     * @param fm
     * @param context
     */
    public WeekPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    /**
     * Initiates and returns a fragment with the right offset
     * @param i
     * @return
     */
    @Override
    public Fragment getItem(int i) {
        //calculate the right offset for the fragment
        int offset = i-OFFSET;
        Bundle b = new Bundle();
        //add the offset to the arguments
        b.putInt("offset", offset);
        //create the fragment
        Fragment frag = new WeekFragment();
        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return ((2*OFFSET) - 1);
    }

    /**
     * Changes the actionbar title when shown week changes
     * @param i
     * @param v
     * @param i2
     */
    @Override
    public void onPageScrolled(int i, float v, int i2) {
        Calendar calendar = Calendar.getInstance();
        //get the offset of the new fragment
        int offset = i-OFFSET;
        //set the global week to the current week
        ((HebrewCalendar)((Activity)context).getApplication()).currentWeek = offset+1;

        //get the right week
        calendar.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
        calendar.add(Calendar.WEEK_OF_YEAR, offset);

        //set up a hebrew date formatter
        HebrewDateFormatter jdf = new HebrewDateFormatter();

        if (Locale.getDefault().getLanguage().equals("iw")){
            jdf.setHebrewFormat(true);
        }

        JewishCalendar jcal = new JewishCalendar(calendar);

        //get the format jewish month and year of the first day of the shown week
        String text = jdf.formatMonth(new JewishDate(calendar)) + " "
                + jcal.getJewishYear();

        //and set it as the main action bar title
        ActionBarAdapter.getCurrentInstance().setMainText(text);
        ActionBarAdapter.getCurrentInstance().setSecondaryText("");
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}

