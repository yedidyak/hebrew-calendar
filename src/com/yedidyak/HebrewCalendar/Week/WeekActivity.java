package com.yedidyak.HebrewCalendar.Week;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.HebrewCalendar.AboutActivity;
import com.yedidyak.HebrewCalendar.ActionBarAdapter;
import com.yedidyak.HebrewCalendar.Day.DayActivity;
import com.yedidyak.HebrewCalendar.HebrewCalendar;
import com.yedidyak.HebrewCalendar.Month.MonthActivity;
import com.yedidyak.HebrewCalendar.R;

public class WeekActivity extends Activity implements ActionBar.OnNavigationListener{

    public final int DAY = 0;
    public final int WEEK = 1;
    public final int MONTH = 2;

    private ViewPager weekPager;
    private WeekPagerAdapter weekAdapter;

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent i;
        switch (item.getItemId()) {
            case R.id.about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                return  true;
            default:
                return false;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        weekAdapter = new WeekPagerAdapter(getFragmentManager(), this);

        ActionBarAdapter actionBarAdapter =  ActionBarAdapter.getInstance(this,
                android.R.layout.simple_dropdown_item_1line,
                getResources().getStringArray(R.array.action_items));

        //long selected = getIntent().getLongExtra("selected", 0);
        weekPager = new ViewPager(this);
        weekPager.setId(R.id.id_pager_week);
        weekPager.setAdapter(weekAdapter);
        weekPager.setOnPageChangeListener(weekAdapter);
        weekPager.setCurrentItem(WeekPagerAdapter.OFFSET-1 + ((HebrewCalendar)getApplication()).currentWeek);

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setListNavigationCallbacks(actionBarAdapter, this);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setSelectedNavigationItem(WEEK);
        rearrange(WEEK);
        actionBar.show();

        setContentView(this.weekPager);
    }

    private void rearrange(int mode){
        Intent i;
        switch (mode){
            case DAY:
                i = new Intent(this, DayActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            case WEEK:
                break;
            case MONTH:
                i = new Intent(this, MonthActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        rearrange(itemPosition);
        return true;
    }

}

