package com.yedidyak.HebrewCalendar.Week;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.yedidyak.HebrewCalendar.Day.ViewDayColumn;
import com.yedidyak.HebrewCalendar.Model.WeekModel;
import com.yedidyak.HebrewCalendar.View.HourCell;

import java.util.ArrayList;
import java.util.List;

public class ViewWeek extends ViewGroup implements SingleWeekView {

    private float hr;
    private float min;
    //private float ss = 0;
    private int width;
    private Paint linePaint;
    private int maxAllDay = 0;

    private int margin;

    private WeekModel weekModel = null;
    private Context context;

    private List<ViewDayColumn> dayColumns = new ArrayList<ViewDayColumn>(7);

    private List<HourCell> hours = new ArrayList<HourCell>();
    private int topMargin;


    //private Line ssLine;

    public ViewWeek(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ViewWeek(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ViewWeek(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init(){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

        linePaint = new Paint();
        linePaint.setStrokeWidth(1);
        linePaint.setColor(Color.LTGRAY);

        Resources r = getResources();
        margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());
        //this.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());
        this.topMargin=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());

        hr = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, r.getDisplayMetrics());
        min = hr/60;

        this.setWillNotDraw(false);

        //this.ssLine = new Line(context);
        //ssLine.setBackgroundColor(Color.RED);
        //this.addView(ssLine);

        for (int i=0; i<24; i++){
            this.addHourCell(i);
        }

        for (int i=0; i<7; i++){
            this.dayColumns.add(i,new ViewDayColumn(context, hr, min));
            //this.dayColumns.get(i).showDayBar();
            this.addView(this.dayColumns.get(i));
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        findAllDaySpaces();
        int t2= t + topMargin * maxAllDay;

        for (HourCell cell: this.hours){
            int top = t2 + cell.hour * (int)hr;
            int bottom = t2 + top + (int)hr;
            int right = l+margin;
            cell.layout(l,top,right,bottom);
        }

        int columnWidth = (r-l-margin)/7;

        for (int i=0; i<7; i++){
            int left = l+margin + (i*columnWidth);
            int right = left+columnWidth;
            this.dayColumns.get(i).setWidth(columnWidth);
            measureChild(this.dayColumns.get(i), getMeasuredWidth(), getMeasuredHeight());
            this.dayColumns.get(i).layout(left, t, right, b );
            /////////////////////////////////////
            //this.dayColumns.get(i).onLayout(true, left, t, right, b );
        }

    }

    public void setWeek(WeekModel week){
        this.weekModel = week;
        for (int i=0; i<7; i++){
            this.dayColumns.get(i).setDay(week.getDay(i));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int t = topMargin * maxAllDay;
        for (int h=0; h< 24; h++){
            float hl = h*hr;
            canvas.drawLine(0,t+hl,margin,t+hl,linePaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(width, (int)hr*24 + topMargin);
    }

    public void addHourCell(int hour){
        HourCell cell = new HourCell(context, hour);
        this.hours.add(cell);
        this.addView(cell);
    }

    private void findAllDaySpaces(){
        for (int i=0; i<7; i++){
            if (this.weekModel != null && this.weekModel.getDay(i).allDayEvents.size() > maxAllDay){
                maxAllDay = this.weekModel.getDay(i).allDayEvents.size();
            }
        }
        //Log.d("YYY","max all days = " + maxAllDay);
        for (int i=0; i<7; i++){
            this.dayColumns.get(i).setAllDaySpaces(maxAllDay);
        }
    }

}


