package com.yedidyak.HebrewCalendar.Week;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;
import com.yedidyak.HebrewCalendar.Model.WeekModel;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class WeekBar extends ViewGroup {

    private WeekModel weekModel = null;
    private Context context;
    private ArrayList<TextView> txtDays = new ArrayList<TextView>(7);

    private int height;
    private int margin;

    public WeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public WeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    public WeekBar(Context context) {
        super(context);
        this.context = context;
    }

    public void setModel(WeekModel model){
        this.weekModel = model;
        init();
        this.requestLayout();
        this.invalidate();
    }

    private void init(){
        for (int i=0; i<7; i++){
            TextView textDay = new TextView(context);
            String text;
            DateFormatSymbols dfs = new DateFormatSymbols(Locale.getDefault());
            String[] dayCodes = dfs.getShortWeekdays();
            text = dayCodes[weekModel.getDay(i).zmanimCalendar.getCalendar().get(Calendar.DAY_OF_WEEK)];
            text = text + " " + weekModel.getDay(i).hebDateBefore;
            textDay.setText(text);
            textDay.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            textDay.setAllCaps(true);
            textDay.setGravity(Gravity.CENTER_HORIZONTAL);
            this.addView(textDay);
            this.txtDays.add(i,textDay);
        }

        Resources r = getResources();
        height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, r.getDisplayMetrics());
        margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int columnWidth = (r-l-margin)/7;
        int left = margin;
        int right = left+columnWidth;

        if (this.weekModel==null){return;}

        for(int i=0; i<7; i++){
            txtDays.get(i).layout(left, t, right, b);
            left+=columnWidth;
            right+=columnWidth;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, height);
    }
}

