package com.yedidyak.HebrewCalendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//TODO - this can be moved and simplified inside each Activity now
/**
 * Singleton
 * Adapter for displaying the right spinner and title for actionbar of calendar activities
 */
public class ActionBarAdapter extends ArrayAdapter{

    //constants for position indexes
    public final int DAY = 0;
    public final int WEEK = 1;
    public final int MONTH = 2;

    private Context context;
    private LayoutInflater inflater;

    private String mainText = "";
    private String secondaryText = "";

    //Singleton
    private static ActionBarAdapter instance = null;

    /**
     * Singleton initiator
     * @param context
     * @param textViewResourceId
     * @param objects
     * @return
     */
    public static ActionBarAdapter getInstance(Context context, int textViewResourceId, String [] objects){
        if (instance != null){return instance;}
        else
        {
            instance = new ActionBarAdapter(context, textViewResourceId, objects);
            return instance;
        }
    }

    /**
     * Gets current instance, no constructor parameters
     * @return
     */
    public static ActionBarAdapter getCurrentInstance(){return instance;}

    /**
     * sets the main text of the actionbar spinner
     * @param mainText
     */
    public void setMainText(String mainText) {
        this.mainText = mainText;
        this.notifyDataSetChanged();
    }

    /**
     * sets secondary text of actionbar spinner
     * @param secondaryText
     */
    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
        this.notifyDataSetChanged();
    }

    /**
     * Constructor
     * @param context
     * @param textViewResourceId
     * @param objects
     */
    private ActionBarAdapter(Context context, int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.inflater = ((Activity)context).getLayoutInflater();
    }

    /**
     * Gets View of spinner title in the actionbar
     * @param position  selected position
     * @param convertView
     * @param parent
     * @return
     */
    @SuppressLint("CutPasteId")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        //if view is null, inflate the right sort of layout
        if(view==null){
            switch(position){
                //if day, we need a two line spinner
                case DAY:
                    view = inflater.inflate(R.layout.two_line_item, parent ,false);
                    break;
                //for week and month, a one line spinner
                case WEEK:
                case MONTH:
                    view = inflater.inflate(android.R.layout.simple_spinner_item, parent ,false);
                    break;
            }
        }

        //init textviews
        TextView main = null;
        TextView secondary = null;
        //TextView main = new TextView(context);
        //TextView secondary = new TextView(context);
        switch (position){
            //if day, main is the bottom one
            case DAY:
                main = (TextView) view.findViewById(android.R.id.text2);
                secondary = (TextView) view.findViewById(android.R.id.text1);
                break;
            //if week or month, main is the only one
            case WEEK:
            case MONTH:
                main = (TextView) view.findViewById(android.R.id.text1);
                secondary = new TextView(context);
                break;

        }


        //set the text sizes dependent on the orientation
        switch (context.getResources().getConfiguration().orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                secondary.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                main.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                secondary.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                main.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                break;
        }

        //set the text of both - secondary only if day is selected
        switch (position){
            case DAY:
                secondary.setText(secondaryText);
                //main.setText(mainText);
            case WEEK:
            case MONTH:
                main.setText(mainText);
                break;
        }

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int screen_width = point.x;

        double width_multi = (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)?
                0.43 : 0.22;

        view.setMinimumWidth((int)(screen_width*width_multi));
        return view;
    }
}

