package com.yedidyak.HebrewCalendar;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Activity showing the About screen
 */
public class AboutActivity extends Activity {


    //constants for Address and Subject lines in feedback emails
    private final static String ADDRESS = "android.yedidyak@gmail.com";
    private final static String SUBJECT = "Feedback from Hebrew Calendar";

    /**
     * onCreate
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        //find views on layout
        TextView txtVersion = (TextView)findViewById(R.id.txtVersion);
        Button btnFeedback = (Button)findViewById(R.id.btnFeedback);

        //set button onclick
        btnFeedback.setOnClickListener(sendFeedback);

        //get the version string
        String str = getString(R.string.version);

        //try and get the application version name and version number and add to the string
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            str = str + " " + pInfo.versionName;
            str = str + " (" + pInfo.versionCode + ")";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //set the built string to be on the textview
        txtVersion.setText(str);

        //set up the action bar
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(getResources().getString(R.string.about) +
                " " + getResources().getString(R.string.app_name));

    }

    /**
     * OnClick for feedback button
     * Launches Email intent
     */
    private View.OnClickListener sendFeedback = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //set up intent
            Intent email = new Intent(Intent.ACTION_SENDTO);
            Uri uri = Uri.parse("mailto:" + ADDRESS);
            email.setData(uri);
            email.putExtra(Intent.EXTRA_SUBJECT, SUBJECT);
            try {
                //if possible, launch the intent to choose email client
                startActivity(Intent.createChooser(email, "Choose an email client from..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(AboutActivity.this, "No email client installed.", Toast.LENGTH_LONG).show();
            }
        }
    };


    // ANALYTICS TRACKING
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }
}

