package com.yedidyak.HebrewCalendar.Preferences.ColorPicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yedidyak.HebrewCalendar.R;


public class ColorPreference extends DialogPreference implements ColorPickerSwatch.OnColorSelectedListener{

    private final int DEFAULT_VALUE = 0xff5d9732;

    private View view = null;
    private ColorPickerDialog mDialog = null;
    private int mCurrentValue = DEFAULT_VALUE;

    public ColorPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }



    @Override
    public View getView(View convertView, ViewGroup parent) {
        View v = super.getView(convertView, parent);
        v.findViewById(R.id.color).setBackgroundColor(mCurrentValue);
        this.view = v;
        return v;
    }


    @Override
    protected void showDialog(Bundle state) {
        int[] colors = getContext().getResources().getIntArray(R.array.colors);
        mDialog = ColorPickerDialog.newInstance(R.string.choose_colour, colors, mCurrentValue, 4, 100);
        mDialog.setOnColorSelectedListener(this);
        mDialog.show(((Activity)getContext()).getFragmentManager(), "dialog");

    }

    @Override
    public void onColorSelected(int color) {
        persistInt(color);
        ((PreferenceActivity)getContext()).setPreferenceScreen(null);
        ((PreferenceActivity)getContext()).addPreferencesFromResource(R.xml.preferences);
    }


    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        Log.d("ZMAN:", "key: " + getKey());
        if (restorePersistedValue) {
            // Restore existing state
            mCurrentValue = this.getPersistedInt(DEFAULT_VALUE);
        } else {
            // Set default state from the XML attribute
            mCurrentValue = (Integer) defaultValue;
            persistInt(mCurrentValue);
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (Integer)a.getColor(index, DEFAULT_VALUE);
    }

    private static class SavedState extends BaseSavedState {
        // Member that holds the setting's value
        // Change this data type to match the type saved by your Preference
        int value;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            // Get the current preference's value
            value = source.readInt();  // Change this to read the appropriate data type
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // Write the preference's value
            dest.writeInt(value);  // Change this to write the appropriate data type
        }

        // Standard creator object using an instance of this class
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {

                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}
