package com.yedidyak.HebrewCalendar;


import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;
import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.util.GeoLocation;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Singleton
 * Holds the application location, and gets a date for sunset for a given date
 */
public class LocationGetter implements LocationListener{

    private static LocationGetter ourInstance = null;
    //private ZmanimCalendar zman = null;
    private Context context = null;
    private GeoLocation geo;

    /**
     * Singleton manager
     * @param context
     * @return
     */
    public static LocationGetter getInstance(Context context) {
        if (ourInstance == null){
            ourInstance = new LocationGetter(context);
        }
        return ourInstance;
    }

    /**
     * Private constructor
     * Gets location
     * @param context
     */
    private LocationGetter(Context context) {
        //Get location and request a single update
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        Criteria crit = new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_LOW);
        lm.requestSingleUpdate(crit, this, null);

        //get all location providers
        List<String> providers = lm.getProviders(true);

        Location l = null;

        //loop through providers, try to get the last location from one
        if (providers != null){
            for (int i=providers.size()-1; i>=0; i--) {
                l = lm.getLastKnownLocation(providers.get(i));
                if (l != null) break;
            }
        }

        //default location
        float lastLong = 0;
        float lastLat = 0;
        float lastAlt = 0;

        geo = new GeoLocation();
        SharedPreferences sp = context.getSharedPreferences("location", 0);

        //if the location was found use it
        if(l != null){
            lastLong = (float)l.getLongitude();
            lastLat = (float)l.getLatitude();
            lastAlt = (float)l.getAltitude();
        }
        //otherwise, get the last known location saved in preferences, or at worst, 0,0,0
        else{
            lastLong = sp.getFloat("longitude", 0);
            lastLat = sp.getFloat("latitude", 0);
            lastAlt = sp.getFloat("elevation", 0);
            Toast.makeText(context, context.getText(R.string.location_warning), Toast.LENGTH_LONG).show();
        }

        //set the GeoLocation
        geo.setLongitude(lastLong);
        geo.setLatitude(lastLat);
        geo.setElevation(lastAlt);

        //init the Zmanim Calendar with the GeoLocation
        //zman = new ZmanimCalendar(geo);
        this.context = context;
    }

    /**
     * Saves the location on location update
     * Updates the zmanim calendar with new location.
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        geo = new GeoLocation();
        geo.setLatitude(location.getLatitude());
        geo.setLongitude(location.getLongitude());
        geo.setElevation(location.getAltitude());
        geo.setTimeZone(TimeZone.getDefault());
        //this.zman.setGeoLocation(geo);

        SharedPreferences.Editor editor = context.getSharedPreferences("location", 0).edit();
        editor.putFloat("latitude", (float) geo.getLatitude());
        editor.putFloat("longitude", (float) geo.getLongitude());
        editor.putFloat("elevation", (float) geo.getElevation());
        editor.commit();
    }

    /**
     * gets sunset time for any calendar
     * @param //calendar
     * @return
     */
    /*public Calendar getSunSet(Calendar calendar){
        zman.setCalendar(calendar);
        Calendar cal = Calendar.getInstance();
        cal.setTime(zman.getSunset());
        return cal;
    }*/

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public GeoLocation getGeoLocation(){
        return geo;
    }
}
