package com.yedidyak.HebrewCalendar.Model;

import android.database.Cursor;
import android.provider.CalendarContract;

public class CalendarModel {

    public static final String[] projection = {
            CalendarContract.Calendars._ID,
            CalendarContract.Calendars.CALENDAR_COLOR,
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME
    };

    private final static int ind_ID = 0;
    private final static int ind_COLOR = 1;
    private final static int ind_NAME = 2;

    private int id = 0;
    private int color = 0;
    private String name = "";

    public CalendarModel(Cursor cursor){
        this.id = cursor.getInt(ind_ID);
        this.color = cursor.getInt(ind_COLOR);
        this.name = cursor.getString(ind_NAME);
    }

    public int getId() {
        return id;
    }

    public int getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
}

