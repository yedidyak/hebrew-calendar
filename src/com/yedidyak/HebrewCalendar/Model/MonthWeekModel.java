package com.yedidyak.HebrewCalendar.Model;

import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.util.Calendar;

public class MonthWeekModel extends WeekModel {

    public boolean[] evenMonth;
    public boolean[] isToday;
    public int JmonthOfFirstDay = 0;

    public MonthWeekModel() {
        this.evenMonth = new boolean[7];
        this.isToday = new boolean[7];
    }

    @Override
    public void setDayModel(DayModel dayModel, int day){
        super.setDayModel(dayModel, day);

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        start.setTime(dayModel.zmanimCalendar.getSunset());
        start.set(Calendar.HOUR_OF_DAY,0);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND, 0);
        end.setTime(start.getTime());
        end.add(Calendar.DAY_OF_YEAR, 1);

        if (now.after(start) && now.before(end)){
            this.isToday[day] = true;
        }
        else{
            this.isToday[day] = false;
        }

        JewishDate jdate = new JewishDate(start);

        if (day == 0){
            this.JmonthOfFirstDay = jdate.getJewishMonth();
        }

        this.evenMonth[day] = jdate.getJewishMonth() % 2 == 0;
    }
}

