package com.yedidyak.HebrewCalendar.Model;

import java.util.ArrayList;
import java.util.List;

public class WeekModel {

    protected List<DayModel> dayModels = new ArrayList<DayModel>(7);

    public WeekModel(){}

    public void setDayModel(DayModel dayModel, int day){
        this.dayModels.add(day, dayModel);
    }

    public DayModel getDay(int day) {
        return this.dayModels.get(day);
    }

    public int getNumOfDays(){
        return dayModels.size();
    }
}

