package com.yedidyak.HebrewCalendar.Model;

import android.database.Cursor;
import android.provider.CalendarContract;

import java.io.Serializable;
import java.util.Calendar;

public class Event implements Serializable {

    public static final String[] projection = {
            CalendarContract.Events._ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DESCRIPTION,
            CalendarContract.Events.EVENT_LOCATION,
            CalendarContract.Events.CALENDAR_DISPLAY_NAME,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.CALENDAR_COLOR,
            CalendarContract.Events.ALL_DAY,
            CalendarContract.Events.CALENDAR_ID
    };

    public static final String[] instances_projection = {
            CalendarContract.Instances._ID,
            CalendarContract.Instances.TITLE,
            CalendarContract.Instances.DESCRIPTION,
            CalendarContract.Instances.EVENT_LOCATION,
            CalendarContract.Instances.CALENDAR_DISPLAY_NAME,
            CalendarContract.Instances.BEGIN,
            CalendarContract.Instances.END,
            CalendarContract.Instances.CALENDAR_COLOR,
            CalendarContract.Instances.ALL_DAY,
            CalendarContract.Instances.CALENDAR_ID
    };

    private final static int ind_ID = 0;
    private final static int ind_TITLE = 1;
    private final static int ind_DESCRIPTION = 2;
    private final static int ind_LOCATION = 3;
    private final static int ind_CALENDAR_NAME = 4;
    private final static int ind_DTSTART = 5;
    private final static int ind_DTEND = 6;
    private final static int ind_COLOR = 7;
    public final static int ind_ALL_DAY = 8;
    public final static int ind_CAL_ID = 9;

    private int ID;
    private String title;
    private String description;
    private String location;
    private String calendar_name;
    private Calendar start;
    private Calendar end;
    private int color;
    private boolean allDay;
    private int cal_ID;

    public Event(){
        this.calendar_name = "";
        this.description = "";
        this.ID = 0;
        this.location = "";
        this.title = "";
        this.start = Calendar.getInstance();
        this.start.setTimeInMillis(0);
        this.end = Calendar.getInstance();
        this.end.setTimeInMillis(0);
        this.color = 0;
        this.allDay = false;
        this.cal_ID = 0;
    }

    public Event(Cursor cursor){
        this.calendar_name = cursor.getString(ind_CALENDAR_NAME);
        this.description = cursor.getString(ind_DESCRIPTION);
        this.ID = cursor.getInt(ind_ID);
        this.location = cursor.getString(ind_LOCATION);
        this.title = cursor.getString(ind_TITLE);
        this.start = Calendar.getInstance();
        this.start.setTimeInMillis(Long.parseLong(cursor.getString(ind_DTSTART)));
        this.end = Calendar.getInstance();
        this.end.setTimeInMillis(Long.parseLong(cursor.getString(ind_DTEND)));
        this.color = Integer.parseInt(cursor.getString(ind_COLOR));
        this.allDay = !(cursor.getString(ind_ALL_DAY).equals("0"));
        this.cal_ID = cursor.getInt(ind_CAL_ID);
    }

    public int getID() {
        return ID;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getCalendar_name() {
        return calendar_name;
    }

    public Calendar getStart() {
        return start;
    }

    public Calendar getEnd() {
        return end;
    }

    public int getColor(){
        return color;
    }

    public boolean isAllDay(){return allDay;}

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCalendar_name(String calendar_name) {
        this.calendar_name = calendar_name;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public int getCal_ID() {
        return cal_ID;
    }

    public void setCal_ID(int cal_ID) {
        this.cal_ID = cal_ID;
    }
}

