package com.yedidyak.HebrewCalendar.Model;

import android.view.View;

import net.sourceforge.zmanim.ZmanimCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DayModel {
    //public Calendar sunset;
    public ZmanimCalendar zmanimCalendar;
    public String hebDateBefore;
    public String hebDateAfter;
    public List<Event> events = new ArrayList<Event>();
    public List<Event> allDayEvents = new ArrayList<Event>();
    private List<View> views = new ArrayList<View>();

    public DayModel(ZmanimCalendar zmanCal, String hebDateBefore, String hebDateAfter){
        //this.sunset = sunset;
        this.zmanimCalendar = zmanCal;
        this.hebDateBefore = hebDateBefore;
        this.hebDateAfter = hebDateAfter;
    }

    public void addEvent(Event event){
        if (event.isAllDay() && !this.allDayEvents.contains(event)){
            if(!this.allDayEvents.contains(event)){
                this.allDayEvents.add(event);
                redrawViews();
            }
        }
       else{
            if(!this.events.contains(event)){
                this.events.add(event);
                redrawViews();
            }
        }
    }

    public List<Event> getEvents() {
        return events;
    }

    public List<Event> getAllDayEvents(){return allDayEvents;}

    public boolean hasAllDayEvent(){
        return this.allDayEvents.size()>0;
    }

    public void addView(View view){
        this.views.add(view);
    }

    public void redrawViews(){
        for (View sdview: views){
            sdview.requestLayout();
        }
    }
}

