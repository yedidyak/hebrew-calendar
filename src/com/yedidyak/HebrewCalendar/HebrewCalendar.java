package com.yedidyak.HebrewCalendar;

import android.app.Application;
import android.preference.PreferenceManager;

/**
 * Application object
 * stores current indexes for calendar activities
 */
public class HebrewCalendar extends Application {

    public int currentDay = 0;
    public int currentWeek = 0;
    public int currentMonth = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        LocationGetter.getInstance(this);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }
}
