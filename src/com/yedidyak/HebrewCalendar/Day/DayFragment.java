package com.yedidyak.HebrewCalendar.Day;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.yedidyak.HebrewCalendar.LocationGetter;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.R;

import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DayFragment extends Fragment {

    private int dayOffset;
    private Context mContext;
    private String date = "";

    private DayModel model;
    private Calendar c_start;
    private Calendar c_end;

    private String hebBefore;
    private String hebAfter;
    //private Calendar sscal;
    private ZmanimCalendar zmanCal;

    private ViewDay viewDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment, null);

        ScrollView scrl = (ScrollView) rootView.findViewById(R.id.scroll_view_day);

        viewDay = new ViewDay(mContext);

        c_start = Calendar.getInstance();
        c_start.add(Calendar.DAY_OF_MONTH, dayOffset);
        c_start.set(Calendar.HOUR_OF_DAY,0);
        c_start.set(Calendar.MINUTE,0);
        c_start.set(Calendar.SECOND,0);
        c_start.set(Calendar.MILLISECOND, 0);

        c_end = Calendar.getInstance();
        c_end.add(Calendar.DAY_OF_MONTH, dayOffset);
        c_end.set(Calendar.HOUR_OF_DAY,23);
        c_end.set(Calendar.MINUTE,59);
        c_end.set(Calendar.SECOND,59);
        c_end.set(Calendar.MILLISECOND,999);


        DateFormat df = android.text.format.DateFormat.getDateFormat(mContext);
        HebrewDateFormatter jdf = new HebrewDateFormatter();

        if (Locale.getDefault().getLanguage().equals("iw")){
            jdf.setHebrewFormat(true);
        }

        //txtDate.setText(df.format(c_start.getTime()) + ", " + jdf.format(new JewishDate(c_start)));
        this.date = df.format(c_start.getTime());

        //String hebBefore = jdf.formatHebrewNumber(new JewishDate(c_start).getJewishDayOfMonth());
        hebBefore = "" + new JewishDate(c_start).getJewishDayOfMonth();

        Calendar tmorrow = Calendar.getInstance();
        tmorrow.add(Calendar.DAY_OF_MONTH, dayOffset+1);
        //String hebAfter = jdf.formatHebrewNumber(new JewishDate(tmorrow).getJewishDayOfMonth());
        hebAfter = "" + new JewishDate(tmorrow).getJewishDayOfMonth();

        zmanCal = new ZmanimCalendar(LocationGetter.getInstance(mContext).getGeoLocation());
        zmanCal.setCalendar(c_end);
        //sscal = LocationGetter.getInstance(getActivity()).getSunSet(c_end);
        //model = new DayModel(sscal, hebBefore, hebAfter);
        //new DayEventGetter(mContext, model).execute(c_start, c_end);

        int height = viewDay.getHeight();

        scrl.addView(viewDay);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        model = new DayModel(zmanCal, hebBefore, hebAfter);
        new DayEventGetter(mContext, model).execute(c_start, c_end);
        viewDay.setDay(model);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        dayOffset = (Integer)getArguments().get("offset");
        mContext = activity;
    }
}

