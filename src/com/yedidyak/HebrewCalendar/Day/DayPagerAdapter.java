package com.yedidyak.HebrewCalendar.Day;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import com.yedidyak.HebrewCalendar.ActionBarAdapter;
import com.yedidyak.HebrewCalendar.HebrewCalendar;
import com.yedidyak.HebrewCalendar.R;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DayPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    public static final int OFFSET = 5001;
    enum Days {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY};
    private Context mContext;

    private int currentOffset;

    public DayPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        int offset = i-OFFSET;
        Bundle b = new Bundle();

        b.putInt("offset", offset);

        Fragment frag = new DayFragment();
        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return ((2*OFFSET) - 1);
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

        //changes the date in actionbar spinner
        Calendar calendar = Calendar.getInstance();
        int offset = i-OFFSET;

        ((HebrewCalendar)((Activity)mContext).getApplication()).currentDay = offset;

        calendar.add(Calendar.DAY_OF_MONTH, offset);
        HebrewDateFormatter jdf = new HebrewDateFormatter();

        currentOffset = offset;

        if (Locale.getDefault().getLanguage().equals("iw")){
            jdf.setHebrewFormat(true);
        }

        String date =  jdf.format(new JewishDate(calendar));
        ActionBarAdapter.getCurrentInstance().setMainText(date);

        Calendar now = Calendar.getInstance();

        int millis1 = (int)now.getTimeInMillis()/ 1000 / 60 / 60 / 24;
        int millis2 = (int)calendar.getTimeInMillis()/ 1000 / 60 / 60 / 24;
        int dayDiff = millis2 - millis1 ;

        DateFormat df = SimpleDateFormat.getDateInstance(DateFormat.SHORT);
        String gregDate = df.format(calendar.getTime());

        switch (dayDiff){
            case 0:
                ActionBarAdapter.getCurrentInstance().setSecondaryText(mContext.getResources().getString(R.string.today_caps)
                    + " " + gregDate);
                break;
            case 1:
                ActionBarAdapter.getCurrentInstance().setSecondaryText(mContext.getResources().getString(R.string.tomorrow_caps)
                        + " " + gregDate);
                break;
            case -1:
                ActionBarAdapter.getCurrentInstance().setSecondaryText(mContext.getResources().getString(R.string.yesterday_caps)
                        + " " + gregDate);
                break;
            default:
                ActionBarAdapter.getCurrentInstance().setSecondaryText(jdf.formatDayOfWeek(new JewishDate(calendar)).toUpperCase(Locale.getDefault())
                        + " " + gregDate);
                break;
        }

    }

    public int getCurrentOffset(){
        return currentOffset;
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}

