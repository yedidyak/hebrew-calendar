package com.yedidyak.HebrewCalendar.Day;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;
import com.tjeannin.apprate.AppRate;
import com.yedidyak.HebrewCalendar.AboutActivity;
import com.yedidyak.HebrewCalendar.ActionBarAdapter;
import com.yedidyak.HebrewCalendar.Day.Zmanim.ZmanimActivity;
import com.yedidyak.HebrewCalendar.HebrewCalendar;
import com.yedidyak.HebrewCalendar.Month.MonthActivity;
import com.yedidyak.HebrewCalendar.Preferences.PrefActivity;
import com.yedidyak.HebrewCalendar.R;
import com.yedidyak.HebrewCalendar.Week.WeekActivity;

import java.util.Calendar;

public class DayActivity extends FragmentActivity implements ActionBar.OnNavigationListener{

    public final int DAY = 0;
    public final int WEEK = 1;
    public final int MONTH = 2;

    private ViewPager dayPager;
    private DayPagerAdapter dayAdapter;

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_day, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent i;
        switch (item.getItemId()) {
            case R.id.about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                return  true;
            case R.id.mi_zmanim:
                i = new Intent(this, ZmanimActivity.class);
                i.putExtra("offset", this.dayAdapter.getCurrentOffset());
                startActivity(i);
                return  true;
            case R.id.mi_prefs:
                i = new Intent(this, PrefActivity.class);
                startActivity(i);
                return  true;
            default:
                return false;
        }
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        new AppRate(this)
                .setMinDaysUntilPrompt(0)
                .setMinLaunchesUntilPrompt(10)
                .setShowIfAppHasCrashed(false)
                .init();

        ActionBarAdapter actionBarAdapter =  ActionBarAdapter.getInstance(this,
                android.R.layout.simple_dropdown_item_1line,
                getResources().getStringArray(R.array.action_items));

        long selected = getIntent().getLongExtra("selected", 0);

        dayPager = new ViewPager(this);


        dayAdapter = new DayPagerAdapter(getFragmentManager(), this);
        dayPager.setId(R.id.id_pager_day);
        dayPager.setAdapter(dayAdapter);
        dayPager.setOnPageChangeListener(dayAdapter);

        if (selected != 0){
            Calendar now = Calendar.getInstance();
            int dayDiff = (int) Math.floor((double) (selected-now.getTimeInMillis()) / (1000 * 60 * 60 * 24));
            dayPager.setCurrentItem(DayPagerAdapter.OFFSET+dayDiff);
        }
        else{
            dayPager.setCurrentItem(DayPagerAdapter.OFFSET + ((HebrewCalendar)getApplication()).currentDay);
        }

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setListNavigationCallbacks(actionBarAdapter, this);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setSelectedNavigationItem(DAY);
        rearrange(DAY);
        actionBar.show();

        setContentView(this.dayPager);
	}

    private void rearrange(int mode){
        Intent i;
        switch (mode){
            case DAY:
                break;
            case WEEK:
                i = new Intent(this, WeekActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            case MONTH:
                i = new Intent(this, MonthActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.dayAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        rearrange(itemPosition);
        return true;
    }
}
