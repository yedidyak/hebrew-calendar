package com.yedidyak.HebrewCalendar.Day;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.R;
import com.yedidyak.HebrewCalendar.View.HourCell;
import com.yedidyak.HebrewCalendar.View.Line;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ViewDay extends ViewGroup {


    private float hr;
    private float min;
    private float ss = 0;
    private int width;
    private Paint linePaint;

    private int margin;
    private int topMargin;
    private Context context;
    private DayModel day = null;

    private ViewDayColumn viewDayColumn;
    private TextView beforeSS;
    private TextView afterSS;
    private List<HourCell> hours = new ArrayList<HourCell>();


    private Line ssLine;

    private OnClickListener showSunset = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //Toast.makeText(context, getResources().getString(R.string.sunset_is_at) + day.sunset.get(Calendar.HOUR) +
            //        ":" + day.sunset.get(Calendar.MINUTE), Toast.LENGTH_SHORT).show();
            DateFormat df = new SimpleDateFormat("h:mm");
            Toast.makeText(context, getResources().getString(R.string.sunset_is_at) + " " + df.format(day.zmanimCalendar.getSunset())
                    , Toast.LENGTH_LONG).show();
        }
    };

    public ViewDay(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init(){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

        linePaint = new Paint();
        linePaint.setStrokeWidth(1);
        linePaint.setColor(Color.LTGRAY);

        Resources r = getResources();
        margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());
        this.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());

        hr = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, r.getDisplayMetrics());
        min = hr/60;

        this.afterSS = new TextView(context);
        this.beforeSS = new TextView(context);
        this.addView(afterSS);
        this.addView(beforeSS);

        this.setWillNotDraw(false);

        this.ssLine = new Line(context);
        ssLine.setBackgroundColor(Color.RED);
        this.addView(ssLine);

        for (int i=0; i<24; i++){
            this.addHourCell(i);
        }

        this.viewDayColumn = new ViewDayColumn(context, hr, min);
        this.addView(viewDayColumn);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int t2 = t + (topMargin * this.day.allDayEvents.size());
        for (HourCell cell: this.hours){
            int top = t2 + cell.hour * (int)hr;
            int bottom = t2 + top + (int)hr;
            int right = l+margin;
            cell.layout(l,top,right,bottom);
        }

        this.beforeSS.layout(r-margin, t2+(int)ss-(int)hr,r, t2+(int)ss );
        this.afterSS.layout(r-margin, t2+(int)ss, r, t2+(int)ss+(int)hr);

        this.ssLine.layout(r-margin, t2+(int)ss - 1, r, t2+(int) ss + 1);

        this.viewDayColumn.setAllDaySpaces(day.allDayEvents.size());
        this.viewDayColumn.setWidth((r-l)-margin-margin);
        measureChild(this.viewDayColumn, getMeasuredWidth(), getMeasuredHeight());

        this.viewDayColumn.layout(l+margin, t, r-margin, b);
    }

    public void setDay(DayModel day){

        this.day = day;
        ss = hr*day.zmanimCalendar.getSunset().getHours() + min*day.zmanimCalendar.getSunset().getMinutes();

        this.beforeSS.setText(this.day.hebDateBefore);
        this.beforeSS.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        this.beforeSS.setTextColor(Color.BLACK);
        this.beforeSS.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
        this.beforeSS.setPadding(2,5,2,5);
        this.beforeSS.setOnClickListener(showSunset);

        this.afterSS.setText(this.day.hebDateAfter);
        this.afterSS.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        this.afterSS.setTextColor(Color.BLACK);
        this.afterSS.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        this.beforeSS.setPadding(2, 5, 2, 5);
        this.afterSS.setOnClickListener(showSunset);

        this.ssLine.setOnClickListener(showSunset);
        this.viewDayColumn.setDay(day);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int t = topMargin * this.day.allDayEvents.size();

        for (int h=0; h< 24; h++){
            float hl = h*hr;
            canvas.drawLine(0,t+hl,margin,t+hl,linePaint);
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int t = topMargin * this.day.allDayEvents.size();
        setMeasuredDimension(width, t+(int)hr*24);
    }

    public void addHourCell(int hour){
        HourCell cell = new HourCell(context, hour);
        this.hours.add(cell);
        this.addView(cell);
    }

}

