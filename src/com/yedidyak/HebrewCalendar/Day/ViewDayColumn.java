package com.yedidyak.HebrewCalendar.Day;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.yedidyak.HebrewCalendar.Event.EditEventActivity;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.Model.Event;
import com.yedidyak.HebrewCalendar.R;
import com.yedidyak.HebrewCalendar.View.EventCell;
import com.yedidyak.HebrewCalendar.View.Line;
import com.yedidyak.HebrewCalendar.View.NewEventCell;
import com.yedidyak.HebrewCalendar.View.TapGestureDetector;
import com.yedidyak.HebrewCalendar.Zmanim;

import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

public class ViewDayColumn extends ViewGroup implements SingleDayView, View.OnTouchListener{

    private float hr;
    private float min;
    private float ss = 0;
    private int allDaySpaces = 0;

    private NewEventCell newEventCell = null;
    private int newEventHour = -1;
    private GestureDetector mGestureDetector;

    private final int PAST_COLOR = 0xFFE6E6E6;

    private int width = 0;
    private int topMargin;

    private boolean texts = false;

    private Paint linePaint;
    private Paint pastPaint;
    private Paint zmanPaint;

    private List<EventCell> events = new ArrayList<EventCell>();
    private List<EventCell> allDayEvents = new ArrayList<EventCell>();
    //private Line ssLine;

    private DayModel day = null;
    private Context context;

    public ViewDayColumn(Context context, float hr, float min) {
        super(context);
        this.context = context;
        init();
        this.hr = hr;
        this.min = min;
    }

    public ViewDayColumn(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ViewDayColumn(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init(){
        linePaint = new Paint();
        linePaint.setStrokeWidth(1);
        linePaint.setColor(Color.LTGRAY);

        pastPaint = new Paint();
        pastPaint.setColor(PAST_COLOR);

        zmanPaint = new Paint();
        zmanPaint.setStrokeWidth(3);


        this.setBackgroundColor(Color.WHITE);

        Resources r = getResources();
        this.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, r.getDisplayMetrics());

        this.setOnTouchListener(this);

        this.setWillNotDraw(false);

        /*this.ssLine = new Line(context);
        ssLine.setBackgroundColor(Color.RED);
        this.addView(ssLine);*/

        this.newEventCell = new NewEventCell(context);
        this.newEventCell.setOnClickListener(newEventClick);
        //this.addView(this.newEventCell);
        this.mGestureDetector = new GestureDetector(context, new TapGestureDetector());
    }

    public void setWidth(int width){
        this.width = width;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int t=topMargin*allDaySpaces;

        if(!texts){createTexts();}

        Calendar now = Calendar.getInstance();
        if(day!=null){
            Calendar start = Calendar.getInstance();
            start.setTime(day.zmanimCalendar.getCalendar().getTime());
            start.set(Calendar.HOUR_OF_DAY,0);
            start.set(Calendar.MINUTE,0);

            Calendar end = Calendar.getInstance();
            end.setTime(start.getTime());
            end.add(Calendar.DAY_OF_MONTH, 1);

            if (now.after(end)){
                this.setBackgroundColor(PAST_COLOR);
            }

            if (now.before(end) && now.after(start)){
                float nowBottom = t+timeToPix(now.getTime());
                canvas.drawRect(0,0,getWidth(),nowBottom,pastPaint);
            }
        }

        for (int h=0; h< 24; h++){
            float hl = h*hr;
            canvas.drawLine(0,t+hl,width,t+hl,linePaint);
        }

        canvas.drawLine(1, 0, 1, getHeight(), linePaint);
        canvas.drawLine(canvas.getWidth()-1, 0, canvas.getWidth()-1, getHeight(), linePaint);

        //measureChild(ssLine, getMeasuredWidth(), getMeasuredHeight());
        //this.ssLine.layout(0, t2+ (int)ss - 1 , r, t2+ (int) ss + 1);
        //canvas.drawLine(0, t+(int)ss, getWidth(), t+(int)ss, zmanPaint);
        drawZmanim(canvas, t);
    }

    private void drawZmanim(Canvas canvas, int t){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        for(Zmanim.Zman zman : new Zmanim().getZmanim(day.zmanimCalendar)){
            if(prefs.getBoolean(zman.showKey, zman.defaultShow)){
                drawLine(canvas, t, prefs.getInt(zman.colorKey, zman.defaultColor), zman.time);
            }
        }
    }

    private void drawLine(Canvas canvas, int offset, int color, Date zman){
        int y = (int)timeToPix(zman) + offset;
        zmanPaint.setColor(color);
        canvas.drawLine(0, y, getWidth(), y, zmanPaint);
    }


    public void setDay(DayModel day){
        day.addView(this);
        this.day = day;
        ss = hr*day.zmanimCalendar.getSunset().getHours() + min*day.zmanimCalendar.getSunset().getMinutes() ;
        this.requestLayout();
    }

    private void createTexts(){
        if (this.day == null){return;}
        for (Event event : day.getEvents()){
            EventCell ev = new EventCell(context, event);
            this.addEventCell(ev);
        }
        for (Event allDayEvent : day.getAllDayEvents()){
            EventCell ev = new EventCell(context, allDayEvent);
            this.addEventCell(ev);
        }
        if (day.getEvents().size() != 0){
            this.texts = true;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int t = topMargin * allDaySpaces;
        //if(showDayBar){t=topMargin;}
        setMeasuredDimension(width, (int)hr*24 + t);

        //Log.d("YYY", "ViewDayColumn.measure");
    }

    @Override
    protected void onLayout (boolean changed, int l, int t, int r, int b){

        int t2 = t;

        for (EventCell cell: this.allDayEvents){
            //Log.d("YYY", "ViewDayColumn.layoutCell-Allday " + cell.getId());
            int top = t2;
            int bottom = t2 + topMargin;
            measureChild(cell, width, getMeasuredHeight());
            cell.layout(0,top,r,bottom);
            t2+=topMargin;
        }

        t2 = allDaySpaces * topMargin;

        //Log.d("YYY", "ViewDayColumn.layout");
        for (EventCell cell: this.events){
           // Log.d("YYY", "ViewDayColumn.layoutCell");
            int top = t2 + (int)timeToPix(cell.event.getStart().getTime());
            int bottom = t2 + (int)timeToPix(cell.event.getEnd().getTime());
            measureChild(cell, width, getMeasuredHeight());
            cell.layout(0,top,r,bottom);
        }

        if(newEventHour!=-1){
            int top = t+(topMargin*allDaySpaces)+(int)(newEventHour*hr);
            int bottom = top + (int)hr;
            measureChild(newEventCell, width, getMeasuredHeight());
            newEventCell.layout(0,top,r,bottom);
        }

        //layoutZmanim();
        //measureChild(ssLine, getMeasuredWidth(), getMeasuredHeight());
        //this.ssLine.layout(0, t2+ (int)ss - 1 , r, t2+ (int) ss + 1);
    }

    private float timeToPix (Date time){
        return hr*time.getHours() + min*time.getMinutes();
    }

    public void addEventCell(EventCell cell){
        if (cell.event.isAllDay())
        {
            boolean exists = false;
            for (EventCell exitingCell : this.allDayEvents){
                if (cell.event.getID() == exitingCell.event.getID()){
                    exists = true;
                    break;
                }
            }
            if(!exists){
                this.allDayEvents.add(cell);
                this.addView(cell);
                requestLayout();
                //Log.d("YYY", "ViewDayColumn.addCell - AllDayCell");
            }
        }

        else if(!this.events.contains(cell)){
            this.events.add(cell);
            this.addView(cell);
            requestLayout();
        }
        //Log.d("YYY", "ViewDayColumn.addCell");
    }

    public void setAllDaySpaces(int spaces){
        this.allDaySpaces = spaces;
        this.requestLayout();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        float modifiedY = event.getY() - (topMargin * allDaySpaces);
        int hourTapped = (int)Math.floor(modifiedY / hr);

        if(mGestureDetector.onTouchEvent(event)){
            this.newEventHour = hourTapped;
            addView(newEventCell);
            requestLayout();
            return false;
        }
        else{
            this.newEventHour = -1;
            removeView(newEventCell);
            requestLayout();
            return true;
        }

    }

    private OnClickListener newEventClick = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(context, EditEventActivity.class);
            Date date = new Date(day.zmanimCalendar.getSunset().getTime());
            date.setHours(newEventHour);
            date.setMinutes(0);
            date.setSeconds(0);
            i.putExtra("date",date.getTime());
            context.startActivity(i);
        }
    };
}

