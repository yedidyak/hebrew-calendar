package com.yedidyak.HebrewCalendar.Day;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import com.yedidyak.HebrewCalendar.Model.DayModel;
import com.yedidyak.HebrewCalendar.Model.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.yedidyak.HebrewCalendar.Util.getTimeInUTC;

public class DayEventGetter extends AsyncTask<Calendar, Event, Void> {

    private Context mContext;
    private DayModel model;

    public DayEventGetter(Context context, DayModel model) {
        this.mContext = context;
        this.model = model;
    }

    @Override
    protected Void doInBackground(Calendar... calendars) {
        Calendar c_start = calendars[0];
        Calendar c_end = calendars[1];

        List<Event> events = new ArrayList<Event>();

        String selection = "((dtstart >= "+(c_start.getTimeInMillis()-2)+") AND (dtend <= "+c_end.getTimeInMillis()+"))";

        Cursor cursor = mContext.getContentResolver().query(
                CalendarContract.Events.CONTENT_URI, Event.projection, selection, null, null
        );


        while (cursor.moveToNext()){
            publishProgress(new Event(cursor));
        }

        cursor.close();

        Calendar midnightUTC = Calendar.getInstance();
        midnightUTC.set(Calendar.DAY_OF_YEAR, c_start.get(Calendar.DAY_OF_YEAR));
        midnightUTC.set(Calendar.YEAR, c_start.get(Calendar.YEAR));
        midnightUTC.set(Calendar.HOUR, 0);
        midnightUTC.set(Calendar.MINUTE,0);
        midnightUTC.set(Calendar.MILLISECOND,0);

        cursor = fetchAllDayCursor(c_start);

        while (cursor.moveToNext()){
            if(cursor.getString(Event.ind_ALL_DAY).equals("1")){
                publishProgress(new Event(cursor));
            }
        }

        cursor.close();

        return null;
    }

    public Cursor fetchAllDayCursor(Calendar start) {
        Calendar end = (Calendar) start.clone();
        end.add(Calendar.DATE, 1);

        long startTime = getTimeInUTC(start) + 1;
        long endTime = getTimeInUTC(end) - 1;

        ContentResolver resolver = mContext.getContentResolver();
        return CalendarContract.Instances.query(resolver, Event.instances_projection, startTime, endTime);
    }

    @Override
    protected void onProgressUpdate(Event... events) {
        super.publishProgress(events);
        this.model.addEvent(events[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}

