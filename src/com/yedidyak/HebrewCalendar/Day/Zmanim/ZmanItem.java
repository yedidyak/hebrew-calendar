package com.yedidyak.HebrewCalendar.Day.Zmanim;

public class ZmanItem {
    public String zman;
    public String label;
    public boolean special = false;

    public ZmanItem(String label, String zman){
        this.label = label;
        this.zman = zman;
    }

    public ZmanItem(String label, String zman, boolean special){
        this(label, zman);
        this.special = special;
    }
}

