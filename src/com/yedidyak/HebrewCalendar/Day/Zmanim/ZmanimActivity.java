package com.yedidyak.HebrewCalendar.Day.Zmanim;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.HebrewCalendar.Day.DayPagerAdapter;

public class ZmanimActivity extends FragmentActivity{

    private ZmanimPagerAdapter adapter;
    private ViewPager dayPager;

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int offset = getIntent().getIntExtra("offset", DayPagerAdapter.OFFSET);

        dayPager = new ViewPager(this);
        dayPager.setId(999);
        adapter = new ZmanimPagerAdapter(getFragmentManager(), this, getActionBar());
        dayPager.setAdapter(adapter);
        dayPager.setOnPageChangeListener(adapter);
        dayPager.setCurrentItem(DayPagerAdapter.OFFSET + offset);

        setContentView(dayPager);
    }
}

