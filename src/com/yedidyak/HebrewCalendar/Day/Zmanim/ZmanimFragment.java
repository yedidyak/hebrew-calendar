package com.yedidyak.HebrewCalendar.Day.Zmanim;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;

import com.yedidyak.HebrewCalendar.LocationGetter;
import com.yedidyak.HebrewCalendar.R;

import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.hebrewcalendar.Daf;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ZmanimFragment extends ListFragment {

    private int offset;
    private ZmanimCalendar zmanCal;
    private HebrewDateFormatter hdf;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        offset = (Integer)getArguments().get("offset");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, offset);

        JewishCalendar jcal = new JewishCalendar(cal);

        zmanCal = new ZmanimCalendar(LocationGetter.getInstance(getActivity()).getGeoLocation());
        zmanCal.setCalendar(cal);

        DateFormat tf = new SimpleDateFormat("h:mm");
        HebrewDateFormatter hdf = new HebrewDateFormatter();
        if(Locale.getDefault().getLanguage().equals("iw")){
            hdf.setHebrewFormat(true);
        }

        List<ZmanItem> zmanim = new ArrayList<ZmanItem>();

        if (jcal.isErevYomTov() || jcal.getDayOfWeek() == Calendar.FRIDAY){
            zmanim.add(new ZmanItem(getString(R.string.candle_lighting), tf.format(zmanCal.getCandleLighting()), true));
        }

        if (jcal.getDayOfOmer() > -1){
            zmanim.add(new ZmanItem(getString(R.string.day_of_omer), ""+jcal.getDayOfOmer(), true));
        }

        if (jcal.getDayOfWeek() == Calendar.SATURDAY){
            if(Locale.getDefault().getCountry().equals("il")){
                jcal.setInIsrael(true);
            }
            else{
                jcal.setInIsrael(false);
            }
            if (!jcal.isYomTov()) {
                zmanim.add(new ZmanItem(getString(R.string.parsha), hdf.formatParsha(jcal), true));
            }
            else
            {
                zmanim.add(new ZmanItem(getString(R.string.parsha), hdf.formatYomTov(jcal), true));
            }
        }


        zmanim.add(new ZmanItem(getString(R.string.dawnLabel), tf.format(zmanCal.getAlosHashachar())));
        zmanim.add(new ZmanItem(getString(R.string.sunriseLabel), tf.format(zmanCal.getSunrise())));
        zmanim.add(new ZmanItem(getString(R.string.sof_shma_mga), tf.format(zmanCal.getSofZmanShmaMGA())));
        zmanim.add(new ZmanItem(getString(R.string.sof_shma_gra), tf.format(zmanCal.getSofZmanShmaGRA())));
        zmanim.add(new ZmanItem(getString(R.string.sof_tfila_mga), tf.format(zmanCal.getSofZmanTfilaMGA())));
        zmanim.add(new ZmanItem(getString(R.string.sof_tfila_gra), tf.format(zmanCal.getSofZmanTfilaGRA())));
        zmanim.add(new ZmanItem(getString(R.string.chatzot), tf.format(zmanCal.getChatzos())));
        zmanim.add(new ZmanItem(getString(R.string.mincha_gedola), tf.format(zmanCal.getMinchaGedola())));
        zmanim.add(new ZmanItem(getString(R.string.mincha_ketana), tf.format(zmanCal.getMinchaKetana())));
        zmanim.add(new ZmanItem(getString(R.string.plag_mincha), tf.format(zmanCal.getPlagHamincha())));
        zmanim.add(new ZmanItem(getString(R.string.sunset), tf.format(zmanCal.getSunset())));
        zmanim.add(new ZmanItem(getString(R.string.tzais), tf.format(zmanCal.getTzais())));
        zmanim.add(new ZmanItem(getString(R.string.tzais_72), tf.format(zmanCal.getTzais72())));


        String strDaf;
        Daf daf = jcal.getDafYomiBavli();

        if(Locale.getDefault().getLanguage().equals("iw")){
            strDaf = daf.getMasechta() + " " + daf.getDaf();
        }
        else{
            strDaf = daf.getMasechtaTransliterated() + " " + daf.getDaf();
        }

        zmanim.add((new ZmanItem(getString(R.string.yomi), strDaf)));

        ZmanimListAdapter adapter = new ZmanimListAdapter(getActivity(), zmanim);
        this.setListAdapter(adapter);

    }
}

