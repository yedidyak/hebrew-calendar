package com.yedidyak.HebrewCalendar.Day.Zmanim;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import com.yedidyak.HebrewCalendar.Day.DayPagerAdapter;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.util.Calendar;
import java.util.Locale;

public class ZmanimPagerAdapter extends DayPagerAdapter {

    private Context mContext;
    private ActionBar actionBar;

    public ZmanimPagerAdapter(FragmentManager fm, Context context, ActionBar actionBar) {
        super(fm, context);
        this.mContext = context;
        this.actionBar = actionBar;
    }

    @Override
    public Fragment getItem(int i) {
        int offset = i-OFFSET;
        Bundle b = new Bundle();

        b.putInt("offset", offset);

        Fragment frag = new ZmanimFragment();
        frag.setArguments(b);

        return frag;
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {
        //changes the date in actionbar spinner
        Calendar calendar = Calendar.getInstance();
        int offset = i-OFFSET;
        calendar.add(Calendar.DAY_OF_MONTH, offset);
        HebrewDateFormatter jdf = new HebrewDateFormatter();

        if (Locale.getDefault().getLanguage().equals("iw")){
            jdf.setHebrewFormat(true);
        }


        String date =  jdf.format(new JewishDate(calendar));

        actionBar.setTitle(date);
        actionBar.show();

    }
}

