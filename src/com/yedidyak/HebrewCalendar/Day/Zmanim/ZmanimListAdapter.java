package com.yedidyak.HebrewCalendar.Day.Zmanim;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.yedidyak.HebrewCalendar.R;

import java.util.List;
import java.util.Locale;

public class ZmanimListAdapter extends BaseAdapter {

    private List<ZmanItem> zmanList;
    private Context context;

    public ZmanimListAdapter(Context context, List<ZmanItem> zmanList){
        this.zmanList = zmanList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return zmanList.size();
    }

    @Override
    public Object getItem(int position) {
        return zmanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //View v = convertView;
        //if(v==null){
           View v = View.inflate(context, R.layout.zman_item, null);
        //}

        TextView txtZman;
        TextView txtLabel;

        if (Locale.getDefault().getLanguage().equals("iw")){
            txtLabel = (TextView)v.findViewById(android.R.id.text2);
            txtZman =  (TextView)v.findViewById(android.R.id.text1);
        }
        else{
            txtLabel = (TextView)v.findViewById(android.R.id.text1);
            txtZman =  (TextView)v.findViewById(android.R.id.text2);
        }

        txtLabel.setText(zmanList.get(position).label + ":");
        txtZman.setText(zmanList.get(position).zman);

        if (zmanList.get(position).special){
            txtLabel.setTextColor(Color.RED);
            txtZman.setTextColor(Color.RED);
            txtLabel.setTypeface(null, Typeface.BOLD);
            txtZman.setTypeface(null, Typeface.BOLD);
        }

        return v;
    }
}

