package com.yedidyak.HebrewCalendar.View;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

public class HourCell extends TextView {
    public HourCell(Context context) {
        super(context);
    }

    public HourCell(Context context, int hour) {
        super(context);
        this.hour = hour;

        String text = ""+(hour%12);
        if (hour == 0){text = "12 AM";}
        if (hour == 12){text = "12 PM";}

        this.setText(text);
        this.setTextColor(Color.BLACK);
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        this.setGravity(Gravity.RIGHT);
        this.setPadding(2,2,5,2);
    }

    public int hour;


}

