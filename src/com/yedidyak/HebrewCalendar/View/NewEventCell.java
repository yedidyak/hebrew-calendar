package com.yedidyak.HebrewCalendar.View;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;
import com.yedidyak.HebrewCalendar.R;

public class NewEventCell extends TextView {
    private Context context;

    public NewEventCell(Context context) {
        super(context);
        this.context = context;
        this.setText(getResources().getText(R.string.new_event));
        this.setBackgroundColor(0xFF3399FF);
        this.setTextColor(Color.WHITE);
    }
}
