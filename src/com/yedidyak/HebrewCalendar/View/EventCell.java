package com.yedidyak.HebrewCalendar.View;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import com.yedidyak.HebrewCalendar.Event.EventActivity;
import com.yedidyak.HebrewCalendar.Model.Event;

public class EventCell extends TextView {

    public Event event;
    private Context context;

    public EventCell(Context context, Event event) {
        super(context);
        this.context = context;
        this.event = event;
        this.setText(event.getTitle());
        this.setBackgroundColor(event.getColor());
        this.setTextColor(Color.WHITE);
        this.setId(event.getID());
        this.setSingleLine(false);
        this.setOnClickListener(clickListener);

    }

    private OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(context, EventActivity.class);
            i.putExtra("event", event);
            //i.putExtra("event_id", event.getID());
            //i.putExtra("allDay", event.isAllDay());
            context.startActivity(i);
        }
    };
}

