package com.yedidyak.HebrewCalendar;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Various functions used by multiple classes
 */
public class Util {
    /**
     * Gets a date, returns the epoch time of that date in UTC timezone
     * @param date
     * @return
     */
    public static long getTimeInUTC(Calendar date) {
        long time = date.getTimeInMillis();
        time += getTimezoneOffset(date.getTimeZone(), time);
        return time;
    }

    /**
     * Used by getTimeInUTC, returns offset for a timezone from a date
     * @param zone
     * @param date
     * @return
     */
    public static long getTimezoneOffset(TimeZone zone, long date) {
        long delta = zone.getOffset(date);
        return delta;
    }
}

