package com.yedidyak.HebrewCalendar.Event;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.HebrewCalendar.LocationGetter;
import com.yedidyak.HebrewCalendar.Model.Event;
import com.yedidyak.HebrewCalendar.R;

import net.sourceforge.zmanim.ZmanimCalendar;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishCalendar;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;
import net.sourceforge.zmanim.util.GeoLocation;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class EventActivity extends Activity {

    private Event event;
    private LinearLayout llEvent;
    private TextView txtTitle;
    private TextView txtTime;
    private TextView txtLocation;
    private TextView txtCalendar;

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent i;
        switch (item.getItemId()) {
            /*case R.id.mi_change_color:
                return true;   */
            case R.id.mi_edit:
                i = new Intent(this, EditEventActivity.class);
                i.putExtra("event", event);
                startActivity(i);
                return true;
            case R.id.mi_trash:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.delete_alert_message))
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getContentResolver().delete(CalendarContract.Events.CONTENT_URI, "_id=" + event.getID(), null);
                                finish();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.cancel), null)
                        .create().show();
            default:
        }
        return false;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_activity);

        //eventId =  getIntent().getIntExtra("event_id", 0);
        //allDay = getIntent().getBooleanExtra("allDay", false);
        this.event = (Event)getIntent().getSerializableExtra("event");


        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.show();

        this.llEvent = (LinearLayout) findViewById(R.id.llEvent);
        this.txtTitle = (TextView) findViewById(R.id.txtTitle);
        this.txtTime = (TextView) findViewById(R.id.txtTime);
        this.txtLocation = (TextView) findViewById(R.id.txtLocation);
        this.txtCalendar = (TextView) findViewById(R.id.txtCalendar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showEvent();
    }

    private void showEvent(){

        /*String selection = "_id=" + eventId;
        Cursor cursor;

        if (allDay){
            cursor = this.getContentResolver().query(
                    CalendarContract.Instances.CONTENT_SEARCH_URI, Event.instances_projection, selection, null, null
            );
        }
        else{
            cursor = this.getContentResolver().query(
                    CalendarContract.Events.CONTENT_URI, Event.projection, selection, null, null
            );
        }


        cursor.moveToFirst();
        this.event = new Event(cursor); */



        this.txtTitle.setText(event.getTitle());
        this.txtLocation.setText(event.getLocation());
        this.llEvent.setBackgroundColor(event.getColor());
        this.txtCalendar.setText(getResources().getText(R.string.calendar) + ": " + event.getCalendar_name());

        String time = "";
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
        HebrewDateFormatter dateFormat = new HebrewDateFormatter();

        GeoLocation location = LocationGetter.getInstance(this).getGeoLocation();

        JewishCalendar start = new JewishCalendar(event.getStart());
        JewishCalendar end = new JewishCalendar(event.getEnd());
        JewishCalendar Jnow = new JewishCalendar(Calendar.getInstance());

        Calendar now = Calendar.getInstance();

        ZmanimCalendar ssCal = new ZmanimCalendar(LocationGetter.getInstance(this).getGeoLocation());
        ssCal.setCalendar(event.getStart());
        if (event.getStart().after(ssCal.getSunset())){
            addDayToJewishCal(start);
        }

        ssCal.setCalendar(event.getEnd());
        if (event.getEnd().after(ssCal.getSunset())){
            addDayToJewishCal(end);
        }

        ssCal.setCalendar(Calendar.getInstance());
        if (Calendar.getInstance().after(ssCal.getSunset())){
            addDayToJewishCal(Jnow);
        }



        if (event.isAllDay()){
            if (start.getJewishYear() == Jnow.getJewishYear() &&
                    start.getJewishMonth() == Jnow.getJewishMonth()
                    && start.getJewishDayOfMonth() == Jnow .getJewishDayOfMonth()){
                time = getResources().getString(R.string.today_u);
            }

            else if (start.getJewishYear() == Jnow.getJewishYear() &&
                    start.getJewishMonth() == Jnow.getJewishMonth() &&
                    start.getJewishDayOfMonth() == Jnow.getJewishDayOfMonth() + 1){
                time = getResources().getString(R.string.tomorrow_u);
            }

            else if (start.getJewishYear() == Jnow.getJewishYear() &&
                    start.getJewishMonth() == Jnow.getJewishMonth() &&
                    start.getJewishDayOfMonth() == Jnow.getJewishDayOfMonth() - 1){
                time = getResources().getString(R.string.yesterday_u);;
            }

            else if (start.getJewishYear() == Jnow.getJewishYear() &&
                    start.getJewishMonth() == Jnow.getJewishMonth() &&
                    start.getJewishDayOfMonth() - Jnow.getJewishDayOfMonth() < 7
                    && start.getJewishDayOfMonth() - Jnow.getJewishDayOfMonth() > 0){
                time = event.getStart().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
            }

            else {time = dateFormat.format(start);}
        }

        else{

            //FROM date
            int dayDiff = event.getStart().get(Calendar.DAY_OF_YEAR) - now.get(Calendar.DAY_OF_YEAR) +
                    365* (event.getStart().get(Calendar.YEAR) - now.get(Calendar.YEAR));

            switch(dayDiff){
                case 0:
                    time = getResources().getString(R.string.today_u);
                    break;
                case 1:
                    time = getResources().getString(R.string.tomorrow_u);
                    break;
                case -1:
                    time = getResources().getString(R.string.yesterday_u);
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    time = event.getStart().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                    break;
                default:
                    time = dateFormat.format(start);
            }

            time = time + " " + getResources().getString(R.string.at) + timeFormat.format(event.getStart().getTime()) + " - ";

            //TO date
            int eventDayLength = end.getJewishDayOfMonth() - start.getJewishDayOfMonth() +
                    30*(end.getJewishMonth() - start.getJewishMonth());

            if (eventDayLength != 0){
                int endDayDiff = event.getEnd().get(Calendar.DAY_OF_YEAR) - now.get(Calendar.DAY_OF_YEAR) +
                        365* (event.getEnd().get(Calendar.YEAR) - now.get(Calendar.YEAR));

                switch(endDayDiff){
                    case 0:
                        time = time + getResources().getString(R.string.today) + " " +
                                getResources().getString(R.string.at);
                        break;
                    case 1:
                        time = time + getResources().getString(R.string.tomorrow) + " " +
                                getResources().getString(R.string.at);
                        break;
                    case -1:
                        time = time + getResources().getString(R.string.yesterday) + " " +
                                getResources().getString(R.string.at);
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        time = time + event.getStart().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
                                + " " + getResources().getString(R.string.at);
                        break;
                }
            }
            time = time + timeFormat.format(event.getEnd().getTime());

        }
        txtTime.setText(time);

    }

    private void addDayToJewishCal(JewishCalendar cal){
        if (cal.getJewishDayOfMonth() == cal.getDaysInJewishMonth()){
            cal.setJewishDayOfMonth(1);
            if (cal.getJewishMonth() == 12 && !cal.isJewishLeapYear()){
                cal.setJewishMonth(1);
            }
            else{
                cal.setJewishMonth(cal.getJewishMonth()+1);
                if(cal.getJewishMonth() == JewishDate.TISHREI){
                    cal.setJewishYear(cal.getJewishYear()+1);
                }
            }
        }
        else{
            cal.setJewishDayOfMonth(cal.getJewishDayOfMonth()+1);
        }
    }
}