package com.yedidyak.HebrewCalendar.Event;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import com.yedidyak.HebrewCalendar.R;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HebrewDatePickerDialog extends DialogFragment implements NumberPicker.OnValueChangeListener{

    private JewishDate date;
    private int tag;
    private HebrewDateFormatter hdf = new HebrewDateFormatter();
    private NumberPicker spinMonth;
    private NumberPicker spinDay;
    private NumberPicker spinYear;
    private int yearOffset = 0;

    private boolean isModal = false;

    public static HebrewDatePickerDialog getInstance(long start, int tag){

        HebrewDatePickerDialog hdialog = new HebrewDatePickerDialog();

        if (Locale.getDefault().getLanguage().equals("iw")){
            hdialog.hdf.setHebrewFormat(true);
        }

        Bundle b = new Bundle();
        b.putLong("start", start);
        b.putInt("tag", tag);
        hdialog.setArguments(b);
        hdialog.isModal = true;
        hdialog.tag = tag;
        return hdialog;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
    }

    public void setDays(){
        List<String> days = new ArrayList<String>();
        for (int day =1; day <= date.getDaysInJewishMonth(); day++) {
            days.add(Integer.toString(day));
        }
        String[] arrDays = days.toArray(new String[0]);

        spinDay.setDisplayedValues(arrDays);
        spinDay.setMaxValue(arrDays.length - 1);
        spinDay.setMinValue(1);
        spinDay.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        spinDay.setValue(date.getJewishDayOfMonth());

    }

    public void setMonths(){
        List<String> months = new ArrayList<String>();

        int maxMonth = date.isJewishLeapYear()? 13: 12;

        JewishDate tempDate = new JewishDate(date.getTime());
        for (int month = 1; month<=maxMonth; month++){
            tempDate.setJewishMonth(month);
            months.add(hdf.formatMonth(tempDate));
        }

        String[] arrMonths = months.toArray(new String[0]);

        spinMonth.setMinValue(0);
        spinMonth.setMaxValue(0);
        spinMonth.setDisplayedValues(arrMonths);
        spinMonth.setMaxValue(arrMonths.length - 1);
        spinMonth.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        spinMonth.setValue(date.getJewishMonth()-1);
    }

    private void setYears(){
        List<String> years = new ArrayList<String>();

        for (int year = date.getJewishYear()-100; year<date.getJewishYear()+100; year++){
            years.add(Integer.toString(year));
        }

        yearOffset = Integer.parseInt(years.get(0));

        String[] arrYears = years.toArray(new String[0]);

        JewishDate now = new JewishDate();


        spinYear.setDisplayedValues(arrYears);
        spinYear.setMaxValue(arrYears.length-1);
        spinYear.setMinValue(0);
        spinYear.setValue(100);
        spinYear.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(isModal) // AVOID REQUEST FEATURE CRASH
        {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        else
        {
            View view = inflater.inflate(R.layout.hebrew_date_dialog, container, false);
            setUpUI(view);
            return view;
        }
    }

    private void setUpUI(View v){
        setStyle(STYLE_NO_FRAME, 0);
        long start = getArguments().getLong("start");
        date = new JewishDate(new Date(start));

        //View v = inflater.inflate(R.layout.hebrew_date_dialog, null);
        spinMonth = (NumberPicker)v.findViewById(R.id.spinMonth);
        spinDay = (NumberPicker)v.findViewById(R.id.spinDay);
        spinYear = (NumberPicker)v.findViewById(R.id.spinYear);
        spinMonth.setOnValueChangedListener(this);
        spinDay.setOnValueChangedListener(this);
        spinYear.setOnValueChangedListener(this);

        setYears();
        setMonths();
        setDays();
        date.setJewishDayOfMonth(spinDay.getValue());
        date.setJewishMonth(spinMonth.getValue()+1);
        date.setJewishYear(Integer.parseInt(spinYear.getDisplayedValues()[spinYear.getValue()]));
    }


    @Override
    public Dialog onCreateDialog(Bundle bundle) {

        final int tag = getArguments().getInt("tag");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.hebrew_date_dialog, null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((EditEventActivity)getActivity()).doPositiveClick(tag, date);
                        }
                    })
                .setTitle(getResources().getText(R.string.set_date))
                .setView(view);

        setUpUI(view);
        return builder.create();

        //return new HebrewDatePickerDialog()
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        if(picker == spinDay){
            date.setJewishDayOfMonth(spinDay.getValue());
        }
        else if (picker==spinMonth){
            date.setJewishMonth(spinMonth.getValue()+1);
            setDays();
        }
        else if (picker==spinYear){
            int month = date.getJewishMonth();
            boolean wasLY = date.isJewishLeapYear();
            date.setJewishMonth(1);
            date.setJewishYear(yearOffset+newVal);
            boolean isLY = date.isJewishLeapYear();

            if(isLY != wasLY){
                if(!isLY && month==13){
                    date.setJewishMonth(12);
                    spinMonth.setValue(11);
                }
                else{
                    date.setJewishMonth(month);
                }
                setMonths();
            }
            else{
                date.setJewishMonth(month);
            }

            setYears();
        }
        getDialog().setTitle(hdf.format(date));
    }

}

