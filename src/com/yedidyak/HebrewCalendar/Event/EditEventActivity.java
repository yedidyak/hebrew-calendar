package com.yedidyak.HebrewCalendar.Event;

import android.app.*;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.google.analytics.tracking.android.EasyTracker;
import com.yedidyak.HebrewCalendar.Day.DayActivity;
import com.yedidyak.HebrewCalendar.Model.CalendarModel;
import com.yedidyak.HebrewCalendar.Model.Event;
import com.yedidyak.HebrewCalendar.R;
import net.sourceforge.zmanim.hebrewcalendar.HebrewDateFormatter;
import net.sourceforge.zmanim.hebrewcalendar.JewishDate;

import java.text.SimpleDateFormat;
import java.util.*;

public class EditEventActivity extends Activity {

    private Event event = new Event();

    private Button btnDateFrom;
    private Button btnDateTo;
    private Button btnTimeFrom;
    private Button btnTimeTo;
    private EditText edtTitle;
    private EditText edtLocation;
    private CheckBox chkAllDay;
    private EditText edtDescription;
    private Spinner spnCalendars;
    private CalendarsAdapter adapter;
    private LinearLayout llEvent;

    private boolean update = false;

    private final int DATE_FROM = 1;
    private final int DATE_TO = 2;
    private final int TIME_FROM = 3;
    private final int TIME_TO = 4;

    private HebrewDateFormatter hdf = new HebrewDateFormatter();
    private SimpleDateFormat timeFormat = new SimpleDateFormat();

    private List<CalendarModel> calendarModels = new ArrayList<CalendarModel>();


    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_event_activity);

        if (Locale.getDefault().getLanguage().equals("iw")){
            hdf.setHebrewFormat(true);
        }

        if (getIntent().hasExtra("event")){
            this.event = (Event)getIntent().getSerializableExtra("event");
            this.update = true;
        }

        else{
            this.event = new Event();
            this.update = false;
            long start = getIntent().getLongExtra("date", 0);
            long end = start+(60*60*1000);
            event.getStart().setTimeInMillis(start);
            event.getEnd().setTimeInMillis(end);
        }

        timeFormat.applyPattern("hh:mm a");

        btnDateFrom = (Button)findViewById(R.id.btnDateFrom);
        btnDateFrom.setOnClickListener(dateClick);
        btnDateFrom.setTag(R.id.tag_id_view, DATE_FROM);

        btnDateTo = (Button)findViewById(R.id.btnDateTo);
        btnDateTo.setOnClickListener(dateClick);
        btnDateTo.setTag(R.id.tag_id_view, DATE_TO);

        btnTimeFrom = (Button)findViewById(R.id.btnTimeFrom);
        btnTimeFrom.setOnClickListener(timeClick);
        btnTimeFrom.setTag(R.id.tag_id_view, TIME_FROM);

        btnTimeTo = (Button)findViewById(R.id.btnTimeTo);
        btnTimeTo.setOnClickListener(timeClick);
        btnTimeTo.setTag(R.id.tag_id_view, TIME_TO);

        edtTitle = (EditText)findViewById(R.id.editEventName);
        edtTitle.setOnEditorActionListener(editorActionListener);

        edtLocation = (EditText)findViewById(R.id.editLocation);
        edtLocation.setOnEditorActionListener(editorActionListener);

        chkAllDay = (CheckBox)findViewById(R.id.chkAllDay);
        chkAllDay.setOnCheckedChangeListener(allDayCheck);

        edtDescription = (EditText)findViewById(R.id.editEventDescription);
        edtLocation.setOnEditorActionListener(editorActionListener);

        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        ContentResolver resolver = getContentResolver();
        Cursor calendarCursor = resolver.query(uri, CalendarModel.projection, null, null, null);
        while(calendarCursor.moveToNext()){
            this.calendarModels.add(new CalendarModel(calendarCursor));
        }
        adapter = new CalendarsAdapter(calendarModels, getLayoutInflater());

        spnCalendars = (Spinner)findViewById(R.id.spinCalendars);
        spnCalendars.setAdapter(adapter);
        spnCalendars.setOnItemSelectedListener(spinListener);

        llEvent = (LinearLayout)findViewById(R.id.llEvent);

        showEvent();

        ActionBar actionBar = getActionBar();
        View actionBarButtons = getLayoutInflater().inflate(R.layout.custom_action_bar_done_cancel,
                new LinearLayout(this), false);
        View cancelActionView = actionBarButtons.findViewById(R.id.action_cancel);
        cancelActionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditEventActivity.this.finish();
            }
        });
        View doneActionView = actionBarButtons.findViewById(R.id.action_done);
        doneActionView.setOnClickListener(mActionBarListener);
        actionBar.setCustomView(actionBarButtons);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.show();
    }

    private void showEvent(){

        btnTimeFrom.setText(timeFormat.format(event.getStart().getTime()));
        btnDateTo.setText(hdf.format(new JewishDate(event.getEnd())));
        btnDateFrom.setText(hdf.format(new JewishDate(event.getStart())));
        btnTimeTo.setText(timeFormat.format(event.getEnd().getTime()));
        edtLocation.setText(event.getLocation());
        edtTitle.setText(event.getTitle());
        edtDescription.setText(event.getDescription());
        event.setCal_ID(((CalendarModel) adapter.getItem(spnCalendars.getSelectedItemPosition())).getId());
        event.setCalendar_name(((CalendarModel) adapter.getItem(spnCalendars.getSelectedItemPosition())).getName());
        llEvent.setBackgroundColor(((CalendarModel)adapter.getItem(spnCalendars.getSelectedItemPosition())).getColor());

        int visible = event.isAllDay()? Button.INVISIBLE : Button.VISIBLE;
        float dateWeight = event.isAllDay()? 5f : 3f;
        float timeWeight = event.isAllDay()? 0f : 2f;
        int timeHeight = event.isAllDay()? 0 : LinearLayout.LayoutParams.WRAP_CONTENT;
        int sxtnDP = (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()));
        int setIfAllDay = event.isAllDay()? sxtnDP : 0;
        int setIfNotAllDay = event.isAllDay()? 0 : sxtnDP;
        LinearLayout.LayoutParams dateParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, dateWeight);
        LinearLayout.LayoutParams timeParams = new LinearLayout.LayoutParams(0, timeHeight, timeWeight);
        dateParams.setMargins(sxtnDP, 0, setIfAllDay, 0);
        timeParams.setMargins(0,0,setIfNotAllDay,0);
        btnTimeFrom.setVisibility(visible);
        btnTimeTo.setVisibility(visible);
        btnDateTo.setLayoutParams(dateParams);
        btnDateFrom.setLayoutParams(dateParams);
        btnTimeTo.setLayoutParams(timeParams);
        btnTimeFrom.setLayoutParams(timeParams);
    }

    private View.OnClickListener dateClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (Integer)v.getTag(R.id.tag_id_view);
            long time = 0;

            switch(tag){
                case DATE_FROM:
                    time = event.getStart().getTimeInMillis();
                    break;
                case DATE_TO:
                    time = event.getEnd().getTimeInMillis();
                    break;
            }

            DialogFragment dialogFragment = HebrewDatePickerDialog.getInstance(time, tag);
            dialogFragment.show(getFragmentManager(), "dateDialog");
        }
    };

    private View.OnClickListener timeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (Integer)v.getTag(R.id.tag_id_view);

            Dialog dialog = null;

            switch(tag){
                case TIME_FROM:
                    dialog = new TimePickerDialog(EditEventActivity.this, AlertDialog.THEME_HOLO_LIGHT,
                            timeFromSet, event.getStart().get(Calendar.HOUR_OF_DAY),
                            event.getStart().get(Calendar.MINUTE), false);
                    break;
                case TIME_TO:
                    dialog = new TimePickerDialog(EditEventActivity.this, AlertDialog.THEME_HOLO_LIGHT,
                            timeToSet, event.getEnd().get(Calendar.HOUR_OF_DAY),
                            event.getEnd().get(Calendar.MINUTE), false);
                    break;
            }

            dialog.show();
        }
    };

    private TimePickerDialog.OnTimeSetListener timeFromSet = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            event.getStart().set(Calendar.HOUR_OF_DAY, hourOfDay);
            event.getStart().set(Calendar.MINUTE, minute);

            if (event.getEnd().before(event.getStart())){
                event.getEnd().set(Calendar.HOUR_OF_DAY, hourOfDay);
                event.getEnd().set(Calendar.MINUTE, minute);
            }
            showEvent();
        }
    };

    private TimePickerDialog.OnTimeSetListener timeToSet = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            event.getEnd().set(Calendar.HOUR_OF_DAY, hourOfDay);
            event.getEnd().set(Calendar.MINUTE, minute);

            if (event.getEnd().before(event.getStart())){
                event.getEnd().add(Calendar.DAY_OF_YEAR, 1);
            }

            showEvent();
        }
    };

    public void doPositiveClick(int tag, JewishDate jdate) {

        Date date = jdate.getTime();

        switch (tag){
            case DATE_FROM:
                date.setHours(event.getStart().get(Calendar.HOUR_OF_DAY));
                date.setMinutes(event.getStart().get(Calendar.MINUTE));
                date.setSeconds(event.getStart().get(Calendar.SECOND));
                event.getStart().setTime(date);

                if (event.getEnd().before(event.getStart())){
                    event.getEnd().set(Calendar.DAY_OF_YEAR, event.getStart().get(Calendar.DAY_OF_YEAR));
                    event.getEnd().set(Calendar.YEAR, event.getStart().get(Calendar.YEAR));
                }

                break;
            case DATE_TO:
                date.setHours(event.getEnd().get(Calendar.HOUR_OF_DAY));
                date.setMinutes(event.getEnd().get(Calendar.MINUTE));
                date.setSeconds(event.getEnd().get(Calendar.SECOND));
                event.getEnd().setTime(date);

                if (event.getEnd().before(event.getStart())){
                    event.getStart().setTimeInMillis(event.getEnd().getTimeInMillis());
                }

                break;
        }
        showEvent();
    }

    private EditText.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_DONE){
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(EditEventActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                event.setTitle(edtTitle.getText().toString());
                event.setLocation(edtLocation.getText().toString());
                event.setDescription(edtDescription.getText().toString());
                showEvent();
                return true;
            }
            return false;
        }
    };

    private CheckBox.OnCheckedChangeListener allDayCheck = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            event.setAllDay(isChecked);
            showEvent();
        } };

    private Spinner.OnItemSelectedListener spinListener = new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            showEvent();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnClickListener mActionBarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            event.setTitle(edtTitle.getText().toString());
            event.setLocation(edtLocation.getText().toString());
            event.setDescription(edtDescription.getText().toString());

            String tz = TimeZone.getDefault().getID();
            if(event.isAllDay()){
                tz = TimeZone.getTimeZone("UTC").getID();
            }

            ContentValues values = new ContentValues();
            //if (update){values.put(CalendarContract.Events._ID, event.getID());}
            values.put(CalendarContract.Events.DTSTART, event.getStart().getTimeInMillis());
            values.put(CalendarContract.Events.DTEND, event.getEnd().getTimeInMillis());
            values.put(CalendarContract.Events.DESCRIPTION , event.getDescription());
            values.put(CalendarContract.Events.EVENT_TIMEZONE, tz);
            values.put(CalendarContract.Events.TITLE, event.getTitle());
            values.put(CalendarContract.Events.EVENT_LOCATION, event.getLocation());
            values.put(CalendarContract.Events.ALL_DAY, event.isAllDay());
            values.put(CalendarContract.Events.CALENDAR_ID, event.getCal_ID());

            ContentResolver resolver = getContentResolver();
            if (update){
                Uri uri = CalendarContract.Events.CONTENT_URI;
                String[] args = new String[]{Integer.toString(event.getID())};
                resolver.update(uri, values, "_id=?", args);
            }
            else {
                Uri uri = resolver.insert(CalendarContract.Events.CONTENT_URI, values);
            }

            Intent i = new Intent(getApplicationContext(), DayActivity.class);
            startActivity(i);

        }
    };

}