package com.yedidyak.HebrewCalendar.Event;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.yedidyak.HebrewCalendar.Model.CalendarModel;

import java.util.List;

public class CalendarsAdapter extends BaseAdapter{

    private List<CalendarModel> calendarModels;
    private LayoutInflater inflater;

    public CalendarsAdapter(List<CalendarModel> calendarModels, LayoutInflater inflater){
        this.calendarModels = calendarModels;
        this.inflater = inflater;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, null);
        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
        text1.setText(calendarModels.get(position).getName());
        return view;
    }

    @Override
    public int getCount() {
        return calendarModels.size();
    }

    @Override
    public Object getItem(int position) {
        return calendarModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(android.R.layout.simple_spinner_item, null);
        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
        text1.setText(calendarModels.get(position).getName());
        return view;
    }
}

